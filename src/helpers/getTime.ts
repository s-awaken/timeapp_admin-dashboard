export const getTime = (date: string | number): string => {
    const hours = new Date(date).getUTCHours();
    const minutes = new Date(date).getUTCMinutes();
    return formatDigits(hours) + ":" + formatDigits(minutes);
};
export const formatDigits = (number: number): string => {
    return number < 10 ? "0" + number : number.toString();
};