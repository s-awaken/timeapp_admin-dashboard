import dayjs from 'dayjs';
import { Session } from '../interfaces/session';


interface Stats {
  dateIntervalType: string;
  people: number;
  earning: number;
};
type Interval = 'day' | 'month' | 'year' | 'date'  
export const getStatsByInterval = (
  sessions: Session[],
  interval: Interval,
): any[] => {
  const data = [...sessions];

  // Go through all dates
  //    sort based on interval and date
  //    sepaarate by interval
  //    add up price & people 
  const indexes = data.map((item: Session, index: number, array: Session[]) => {
    return {
      index,
      self: array[index],
      split:
        dayjs(item.startTime).get(interval === 'month'
          ? 'month'
          : interval === 'year'
            ? 'year'
            : interval === 'day'
              ? 'day'
              : 'date'
        )
    }
  })
  indexes.sort((indexA: {
    index: number,
    self: Session,
    split: number,
  }, indexB: {
    index: number,
    self: Session,
    split: number,
  }) => {
    return indexA.split > indexB.split ? -1 : 1;
  })
  const indexed = indexes.map((index: {
    index: number,
    self: Session,
    split: number,
  }) => {
    return index.split;
  }).filter(onlyUnique);
  const stats = indexed.map((num: number) => {
    let statData: Session[] = [];
    indexes.map((index: {
      index: number,
      self: Session,
      split: number,
    }) => {
      if (num === index.split) {
        return statData[index.index] = index.self;
      };
    })
    return {
      id: num,
      price: statData.map((stat: Session) => {
        return stat.price;
      }).filter(Number),
    }
  });
  return stats;
};

const onlyUnique = (value: number, index: number, self: any) => {
  return self.indexOf(value) === index;
}