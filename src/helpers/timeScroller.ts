import dayjs from "dayjs"


export const timeScroller = (time: string): {hour: number, minute: number}[] => {
  const times: {hour: number, minute: number}[] = [];
  const timeHours = dayjs(time).get('hour');
  const timeMinutes = 0;
  for (let hourTime = timeHours; hourTime < 24; hourTime++){
    for (let minuteTime = timeMinutes; minuteTime < 4; minuteTime++){
      const returnedTimes = {
        hour: 0,
        minute: 0
      }
      if (minuteTime + 15 === 60) {
        hourTime++
        returnedTimes.hour = hourTime
        returnedTimes.minute = 0
      } else {
        returnedTimes.hour = hourTime
        returnedTimes.minute = minuteTime + 15
      }
      console.log(returnedTimes)
      times.push(returnedTimes)
    }
  }
  return times
}
