import React from 'react';
import ReactDOM from 'react-dom';
import './index.scss';
import AppWrapper from './utils/AppWrapper';
import App from './App';
import reportWebVitals from './reportWebVitals';
import { initializeApp } from "firebase/app";
import { getAnalytics } from "firebase/analytics";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// For Firebase JS SDK v7.20.0 and later, measurementId is optional
const firebaseConfig = {
  apiKey: "AIzaSyBrH3bFMcrMiNzLdYdWrFXws80pGKPgF3g",
  authDomain: "timeapp-admin-dashboard.firebaseapp.com",
  projectId: "timeapp-admin-dashboard",
  storageBucket: "timeapp-admin-dashboard.appspot.com",
  messagingSenderId: "544837330464",
  appId: "1:544837330464:web:8e1128c380f608fb517e45",
  measurementId: "G-NDGP0314YW"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);
const analytics = getAnalytics(app);


ReactDOM.render(
    <AppWrapper>
      <App />
    </AppWrapper>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
