import React from 'react';
import { useCustomEventListener } from 'react-custom-events';

import useLocalStorage from './hooks/useLocalStorage';

import Login from './pages/Login'
import Main from './pages/Index'


const App: React.FC = () => {
  const [loading, setLoading] = React.useState<boolean>(false);
  const [store] = useLocalStorage();
  useCustomEventListener('login-loading', (data: boolean) => 
    setLoading(data)
  );
if (!store.getItem("JWT")) return <Login />
if (loading) return (
  <></>
  );
  return (
    <Main />
  );
}

export default App;
