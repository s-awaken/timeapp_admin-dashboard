import * as React from 'react';
import { useCustomEventListener } from 'react-custom-events';
import { useQuery } from '@apollo/client';
import { ReactComponent as LogoFooter } from '../images/logo_footer.svg';
import { ReactComponent as HomeSVG } from '../images/Home.svg';
import { ReactComponent as HomeBlueSVG } from '../images/HomeBlue.svg';
import { ReactComponent as DashboardSVG } from '../images/GridFeedCards.svg';
import { ReactComponent as DashboardBlueSVG } from '../images/DashboardBlue.svg';
import { ReactComponent as CalendarSVG } from '../images/Calendar.svg';
import { ReactComponent as CalendarBlueSVG} from '../images/CalendarBlue.svg';
import { ReactComponent as SettingSVG } from '../images/Settings.svg';
import { ReactComponent as SettingBlueSVG } from '../images/SettingsBlue.svg';
import { GET_INSTITUTION } from '../graphql/query/getInstitution';
import { GET_INSTITUTION_WORKER } from '../graphql/query/getInstitutionWorker';
import { GET_OWNER } from '../graphql/query/getOwner';
import useTabSwitch from '../hooks/useTabSwitch';
import useLocalStorage from '../hooks/useLocalStorage';
import { useInstitution } from '../utils/recoil/instituition.hook';
import { useOwner } from '../utils/recoil/owner.hook';
import Main from './Main';
import Club from './Club';
import HistoryPage from './History';
import SettingsPage from './Settings';
import ViewClubWorker from './ClubWorker';

const menuList = [
  {
    icon: (<HomeSVG />),
    active: (<HomeBlueSVG />),
  },
  
  {
    icon: (<DashboardSVG />),
    active: (<DashboardBlueSVG/>)
  },
  {
    icon: (<CalendarSVG />),
    active: (<CalendarBlueSVG />)
  },
  // {
  //   icon: (<ChatSVG />),
  // },
  {
    icon: (<SettingSVG />),
    active: (<SettingBlueSVG/>)
  }
]

const DrawerContent = [
  {
    component: (<Main />)
  },
  {
    component: (<Club />)
  },
  {
    component: (<HistoryPage />)
  },
  // {
  //   component: (<ChatPage />)
  // },
  {
    component: (<SettingsPage />)
  },
];
const DrawerContentWorker = [
  {
    component: (<Main />)
  },
  {
    component: (<ViewClubWorker />)
  },
  {
    component: (<HistoryPage />)
  },
  // {
  //   component: (<ChatPage />)
  // },
  {
    component: (<SettingsPage />)
  },
];

export default function PersistentDrawerLeft() {
  const [store] = useLocalStorage();
  const [institutions, { setInstitution }] = useInstitution();
  const [owner, { setOwner }] = useOwner();
  const [selectedIndex, setSelectedIndex] = React.useState(0);
  const [loading, setLoading] = React.useState<boolean>(false);
  const [error, setError] = React.useState<any>();

  const handleListItemClick = (
    index: number,
  ) => {
    store.setItem('PageID', index.toString());
    setSelectedIndex(index);
    changeTab(parseInt(store.getItem('PageID') || "0"))
  };
  React.useEffect(() => {
    changeTab(parseInt(store.getItem('PageID') || "0"));
    setSelectedIndex(parseInt(store.getItem('PageID') || "0"));
  }, [])

  const {
    loading: institutionLoading,
    error: institutionError
  } = useQuery(GET_INSTITUTION, {
    context: {
      headers: {
        Authorization: store.getItem("JWT"),
      }
    },
    onCompleted: (data) => {
      setInstitution(data?.getInstitutionsOfOwner)
    }
  });
  const {
    data: instWorkerData,
    loading: instWorkerLoading,
    error: instWorkerError
  } = useQuery(GET_INSTITUTION_WORKER, {
    context: {
      headers: {
        Authorization: store.getItem("JWT"),
      }
    },
    onCompleted: (data) => {
      setInstitution([data.getInstitutionOfWorker])
    }
  })
  const {
    loading: ownerLoading,
    error: ownerError
  } = useQuery(GET_OWNER, {
    context: {
      headers: {
        Authorization: store.getItem("JWT"),
      }
    },
    onCompleted: (data) => {
      setOwner(data?.getOwner)
    }
  });
  const tabToRender = store.getItem("USER") === "Owner" ? DrawerContent : DrawerContentWorker

  const [tab, changeTab] = useTabSwitch(tabToRender)
  // TODO: Put all event listeners in one function
  // -- Eventhandlers | Start
  useCustomEventListener('create-club-success', (data: any) => {
    window.alert('Клуб создан успешно!')
  });
  useCustomEventListener('create-club-error', (data: any) => {
    window.alert('Ошибка создания клуба!')
  });
  useCustomEventListener('edit-club-success', (data: any) => {
    window.alert('Клуб обновлён успешно!')
  });
  useCustomEventListener('edit-club-error', (data: any) => {
    window.alert('Ошибка обновления клуба!!')
  });
  useCustomEventListener('add-worker-success', (data: any) => {
    window.alert('Сотрудник добавлен!')
  });
  useCustomEventListener('add-worker-error', (data: any) => {
    window.alert('Сотрудник добавлен!')
  });
  useCustomEventListener('session-end-error', (data: any) => {
    window.alert('Ошибка завершения')
  })
  useCustomEventListener('session-end-success', (data: any) => {
    window.alert('Сессия завершена!')
  })
  useCustomEventListener('login-success', (data: boolean) => {
    if (data) window.location.reload();
  })
  useCustomEventListener('loading', (data: boolean) => {
    setLoading(data);
  });
  useCustomEventListener('error', (data) => {
    setError(data);
  })
  // -- Eventhandlers | End

  // Actual Page
  const drawer = (
    menuList.map((item, index: number) => (
      <div className="Item" key={index} onClick={() => handleListItemClick(index)}>
        {selectedIndex === index ? item.active : item.icon}
      </div>
    ))
  )
  return (
    <div className="App">
      <div className="Menu">
        {drawer}
      </div>
      <div className="Content">
        {loading ? (<></>) :
          tab
        }
      </div>
      <div className="Footer">
        <div className="Logo">
          <LogoFooter />
          <p>
            &copy;
            TimeApp, 2021
          </p>
        </div>
        <div className="Phone">
          <p className="Number">
            +7 775 989 91 73
          </p>
          <p>
            Служба поддержки
          </p>
        </div>
      </div>
    </div>
  );
};