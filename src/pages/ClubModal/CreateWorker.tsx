import React from 'react';
import { Box, Button, Typography, Stack, TextField, IconButton } from '@mui/material';
import CloseIcon from '@mui/icons-material/Close';
import { emitCustomEvent } from 'react-custom-events';

import CustomSelect from '../../components/CustomSelect';
import { useMutation } from '@apollo/client';
import { ADD_WORKER } from '../../graphql/mutations/addWorker';
import useLocalStorage from '../../hooks/useLocalStorage';
import { Institution } from '../../interfaces/institution';

interface Props {
  setOpen: () => void;
  institutions: Institution[];
}
interface WorkerData {
  login: string;
  fullName: string;
  password: string;
}
const CreateWorker = ({ setOpen, institutions }: Props) => {
  const [store] = useLocalStorage();
  const [addWorkerUploadData, setAddWorkerUploadData] = React.useState<WorkerData>({
    login: "",
    fullName: "",
    password: "",
  })
  const [selectedInstituition, setSelectedInstitution] = React.useState<string>(institutions[0]?.name);
  const [addWorker, { loading, error }] = useMutation(ADD_WORKER, {
    context: {
      headers: {
        Authorization: store.getItem('JWT'),
      }
    },
    onCompleted: (data) => {
      emitCustomEvent('add-worker-success', data);
      window.location.reload();
    },
    onError: (data) => {
      emitCustomEvent('add-worker-error', data);
      window.location.reload();
    },
  });
  const checkData = (): boolean => {
    return addWorkerUploadData.login.length >= 1 && addWorkerUploadData.fullName.length >= 1 && addWorkerUploadData.password.length >= 4 ? true : false;
  }
  const handleAddWorker = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    addWorker({
      variables: {
        login: addWorkerUploadData.login,
        fullName: addWorkerUploadData.fullName,
        password: addWorkerUploadData.password,
        institutionId: institutions.find(institution => institution.name === selectedInstituition)?._id,
      }
    })
  };
  if (loading) emitCustomEvent('loading', loading);
  emitCustomEvent('loading', false);
  if (error) emitCustomEvent('error', error);
  return (
    <Stack
      component="form"
      direction="column"
      spacing={2}
      justifyContent="center"
      className="Big-Blur"
      sx={{
        width: '365px',
        height: '465.88px',
        px: '20px',
        py: '20px',
      }}
    >
      <Stack direction="row" justifyContent="space-between">
        <Typography
          sx={{
            fontWeight: '600',
            fontSize: '24px',
            lineHeight: '29px',
            my: '6px'
          }}>
          Новый сотрудник
        </Typography>
        <IconButton
          onClick={() => setOpen()}>
          <CloseIcon />
        </IconButton>
      </Stack>
      <input 
        id="fullName"
        name="fullName"
        required
        style={{
          padding: '10px 20px',
          width: "325px",
          height: '65px',
          backgroundColor: '#f7f7f7',
          borderRadius: '15px',
          outline: 'none',
          border: 'none',
          fontSize: '18px',
          lineHeight: '22px',
        }}
        onChange={(event: any) => setAddWorkerUploadData(prevState => ({
          ...prevState,
          fullName: event.target.value,
        }))}
        placeholder="Имя и Фамилия"
        />
      <input 
        id="fullName"
        name="fullName"
        required
        style={{
          padding: '10px 20px',
          width: "325px",
          height: '65px',
          backgroundColor: '#f7f7f7',
          borderRadius: '15px',
          outline: 'none',
          border: 'none',
          fontSize: '18px',
          lineHeight: '22px',
        }}
        onChange={(event: any) => setAddWorkerUploadData(prevState => ({
          ...prevState,
          login: event.target.value,
        }))}
        placeholder="Логин"
        />
      <input 
        id="fullName"
        name="fullName"
        type="password"
        required
        style={{
          padding: '10px 20px',
          width: "325px",
          height: '65px',
          backgroundColor: '#f7f7f7',
          borderRadius: '15px',
          outline: 'none',
          border: 'none',
          fontSize: '18px',
          lineHeight: '22px',
        }}
        onChange={(event: any) => setAddWorkerUploadData(prevState => ({
          ...prevState,
          password: event.target.value,
        }))}
        placeholder="Пароль"
        />
      <CustomSelect
        items={institutions.map(institution => institution.name)}
        selectItems={(item: string) => setSelectedInstitution(item)}
        selectedItem={selectedInstituition} />
      <Button
        disabled={!checkData()}
        sx={{
        width: '365px',
        height: '60px',
        p: '20px 30px',
        backgroundColor: !checkData() ? '#F0F0F0' : '#007AFF',
        borderRadius: '15px',
        color: '#CCCCCC',
        textTransform: 'lowercase',
        }}
        onClick={(e: any)=> handleAddWorker(e)}
      >
        <p style={{ textTransform: 'capitalize', fontSize: '16px', lineHeight: '19px' }}>
          Добавить
        </p>
      </Button>
    </Stack>
  )
}
export default CreateWorker;