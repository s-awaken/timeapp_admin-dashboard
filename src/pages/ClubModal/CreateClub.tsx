import React ,{ Dispatch, SetStateAction }  from 'react';
import { Box, Button, Typography, Stack, Checkbox, Divider, Switch, FormControl, IconButton, Tooltip } from '@mui/material';
import TimePicker from 'react-time-picker';
import ArrowBackRoundedIcon from '@mui/icons-material/ArrowBackRounded';
import DeleteIcon from '@mui/icons-material/Delete';
import PhoneIcon from '@mui/icons-material/Phone';
import { emitCustomEvent } from 'react-custom-events';

import { ReactComponent as TelegramIcon } from '../../images/Specs/telegram.svg';
import { ReactComponent as InstagramIcon } from '../../images/Specs/instagram.svg';
import {ReactComponent as WhatsAppIcon} from '../../images/Specs/whatsapp.svg'
import { ReactComponent as UploadImage } from '../../images/photo_upload.svg';
import { ReactComponent as DescriptionSVG } from '../../images/description.svg';
import { ReactComponent as LocationSVG } from '../../images/location.svg';
import { ReactComponent as VideoUploadSVG } from '../../images/video_upload.svg';
import { ReactComponent as WorkingHoursSVG } from '../../images/working_hours.svg';
import CustomTimeInput from '../../components/CustomTimeInput';
import axios from 'axios';
import useLocalStorage from '../../hooks/useLocalStorage';
import { GET_INSTITUTION_TYPES } from '../../graphql/query/getInstitutionType';
import { GET_TAGS } from '../../graphql/query/getTags';
import { useQuery } from '@apollo/client';
import { InstitutionTypes, AllowedInstitutionTypes } from '../../interfaces/institutionType';
import { Tags } from '../../interfaces/tags';
import WorkTimesContainer from '../../components/WorkTimes';
import dayjs from 'dayjs';
import { Time } from '../../interfaces/times';
import { LocalConvenienceStoreOutlined } from '@mui/icons-material';


interface Props {
  setOpen: () => void;
}
interface CreateClubData {
  address: string,
  avatar: null,
  averagePrice: number,
  city: string,
  description: string,
  email: string,
  gallery: null[],
  instagram: string,
  latitude: number,
  longitude: number,
  name: string,
  phoneNumber: string,
  type: AllowedInstitutionTypes,
  tags: Tags[],
  videoURL: string,
  whatsApp: string,
  workTimes: { startTime: string, endTime: string }[],
};

const addClubQuery = "mutation($address:String!,$avatar:Upload!,$averagePrice:Int!,$city:String!,$description:String!,$email:String!,$gallery:[Upload],$instagram:String,$latitude:Float!,$longitude:Float!,$name:String!,$phoneNumber:String!,$tags:[String!],$type:AllowedInstitutionTypes!,$videoURL:String,$whatsApp:String,$workTimes: [StartEndTimeInput!]!){createInstitution(address:$address,avatar:$avatar,averagePrice:$averagePrice,city:$city,description:$description,email:$email,gallery: $gallery,instagram:$instagram,latitude:$latitude,longitude:$longitude,name:$name,phoneNumber:$phoneNumber,type:$type,tags:$tags,videoURL:$videoURL,whatsApp:$whatsApp,workTimes:$workTimes){_id}}";

export default function CreateClubD({ setOpen }: Props) {
  const [store] = useLocalStorage();
  const selectedTimes = React.useMemo(() => {
    const selectedTime: Time[] = [
      {
        endTime: new Date(new Date().setHours(22, 0, 0)).toISOString(),
        startTime: new Date(new Date().setHours(10, 0, 0)).toISOString()
      },
      {
        endTime: new Date(new Date().setHours(22, 0, 0)).toISOString(),
        startTime: new Date(new Date().setHours(10, 0, 0)).toISOString()
      },
      {
        endTime: new Date(new Date().setHours(22, 0, 0)).toISOString(),
        startTime: new Date(new Date().setHours(10, 0, 0)).toISOString()
      },
      {
        endTime: new Date(new Date().setHours(22, 0, 0)).toISOString(),
        startTime: new Date(new Date().setHours(10, 0, 0)).toISOString()
      },
      {
        endTime: new Date(new Date().setHours(22, 0, 0)).toISOString(),
        startTime: new Date(new Date().setHours(10, 0, 0)).toISOString()
      },
      {
        endTime: new Date(new Date().setHours(22, 0, 0)).toISOString(),
        startTime: new Date(new Date().setHours(10, 0, 0)).toISOString()
      },
      {
        endTime: new Date(new Date().setHours(22, 0, 0)).toISOString(),
        startTime: new Date(new Date().setHours(10, 0, 0)).toISOString()
      }
    ];
    return selectedTime
  }, [])
  const [selectTime, setSelectedTime] = React.useState<Time>(
    {
      endTime: new Date(new Date().setHours(22, 0, 0)).toISOString(),
      startTime: new Date(new Date().setHours(10, 0, 0)).toISOString()
    })
  const [createClubData, setCreateClubData] = React.useState<CreateClubData>({
    address: "",
    avatar: null,
    averagePrice: 0,
    city: "Алматы",
    description: "",
    email: "",
    gallery: [null],
    instagram: "",
    latitude: 0,
    longitude: 0,
    name: "",
    phoneNumber: "",
    type: "Fitness",
    tags: [{
      _id: "",
      name: "",
      iconSVGPath: "",
    }],
    videoURL: "",
    whatsApp: "",
    workTimes: selectedTimes,
  })
  const [fileSelected, setFileSelected] = React.useState<File | null>(null)
  const [filesToUpload, setFilesToUpload] = React.useState<File[]>([]);
  const [openTypes, setOpenTypes] = React.useState<boolean>(false);
  const [openAddress, setOpenAddress] = React.useState<boolean>(false);
  const [workTimesActive, setWorkTimesActive] = React.useState<boolean>(false);
  const [weekendWorkTimesActive, setWeekendWorkTimesActive] = React.useState<boolean>(false);
  const [tags, setTags] = React.useState<Tags[]>();
  const {
    data: typesData,
    loading: typesLoading,
    error: typesError
  } = useQuery(GET_INSTITUTION_TYPES, {
    context: {
      headers: {
        'Authorization': store?.getItem('JWT'),
      }
    },
  });
  const {
    data: tagsData,
    loading: tagsLoading,
    error: tagsError
  } = useQuery(GET_TAGS, {
    context: {
      headers: {
        'Authorization': store?.getItem('JWT'),
      }
    },
    variables: {
      "institutionType": createClubData.type
    },
    onCompleted: (data) => {
      setTags(data?.getTagsByInstitutionTypes)
    }
  })
  const handleSelectTimes = (index: number) => {
    if (index === 7) {
      for (let i = 0; i < 5; i++) {
        selectedTimes[i] = selectTime
      }
    } else if (index === 8) {
      for (let i = 5; i < 7; i++) {
        selectedTimes[i] = selectTime
      }
    } else {
      selectedTimes[index] = selectTime
    }
  }
  const handleImageChange = function (e: React.ChangeEvent<HTMLInputElement>) {
    const fileList = e.target.files;

    if (!fileList) return;
    setFileSelected(fileList[0]);
      
    let duplicateFile;
    if (fileList.length > 0) {
      duplicateFile = filesToUpload.find((doc) => doc.name === fileList[0].name);
      if (!duplicateFile) {
        const currentFiles = filesToUpload;
        currentFiles.push(fileList[0]);
        setFilesToUpload(currentFiles);
      }
    }
  }
  const handleAddClub = (event: React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    const reqData = new FormData();
    setCreateClubData(prevState => ({
      ...prevState,
      gallery: filesToUpload.slice(1).map(() => null),
    }))
    const mapToMap = (length: number) => {
      const final = {
        avatar: ['variables.avatar'],
      }
      const gallery: any = {};
      for (let i = 0; i < (length - 1); i++) {
        setCreateClubData(prevState => ({
          ...prevState,
          gallery: [...prevState.gallery, null]
        }));
        gallery[`gallery${i}`] = [`variables.gallery.${i}`]
      }
      return Object.assign(final, gallery)
    }
    const operations = {
      query: addClubQuery,
      variables: createClubData,
    }
    reqData.set('operations', JSON.stringify(operations));
    reqData.set('map', JSON.stringify(mapToMap(filesToUpload?.length)));
    reqData.set('avatar', filesToUpload[0] || "");
    filesToUpload?.splice(1).map((x, index: number) => {
      reqData.set(`gallery${index}`, x as Blob)
    })
    axios.post('http://167.172.162.8:3001/graphql', reqData, {
      headers: {
        'Content-Type': 'multipart/form-data',
        'Authorization': store.getItem('JWT') || "",
        'Access-Control-Allow-Origin': '*',
      },
    }
    ).then(response => {
      emitCustomEvent('create-club-success', response);
      window.location.reload();
    }).catch(error => {
      emitCustomEvent('create-club-error', error);
    });
  };
  const handleTags = (add: boolean, index: number) => {
    if (!tags) return;
    const tagToFind = tags[index];
    const foundTag = createClubData.tags.indexOf(createClubData.tags.find((tag: Tags) => tag?._id === tagToFind?._id) || {_id: '', name: "", iconSVGPath: ""});
    if (add) { 
      setCreateClubData(prevState => ({
        ...prevState,
        tags: [...prevState.tags, tagToFind]
      }))
    } else {
      setCreateClubData(prevState => ({
        ...prevState,
        ...prevState.tags.splice(foundTag, 1)
      }))
    }
    if (createClubData?.tags[0]?._id === "") createClubData?.tags?.splice(0, 1)
    console.log(createClubData.tags)
  }
  if (typesLoading || tagsLoading) emitCustomEvent('loading', typesLoading || tagsLoading);
  emitCustomEvent('loading', false);
  if (typesError || tagsError) emitCustomEvent('error', typesError || tagsError);
  const institutionTypes: InstitutionTypes[] = typesData?.getInstitutionTypes || [];
  return (
    <Stack
      component="form"
      onSubmit={handleAddClub}
      direction="column"
      alignItems="flex-start"
      justifyContent="flex-start"
      spacing={4}
      sx={{
        width: '100%',
      }}>
      {/* Header */}
      <Stack
        direction="row"
        justifyContent="space-between"
        className="Big-Blur"
        sx={{
          width: '100%',
          py: '10px',
          px: '30px',
        }}>
        <Box sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          justifyContent: 'center',
        }}>
          <Button
            disableFocusRipple
            disableRipple
            sx={{
              textTransform: 'lowercase',
              color: '#007AFF',
              width: '78px',
              backgroundColor: 'transparent',
              "&:hover": {
                backgroundColor: 'transparent',
              }
            }}
            onClick={() => setOpen()}>
            <ArrowBackRoundedIcon
              sx={{
                width: '24px',
                height: '24px'
              }} />
            <p style={{
              textTransform: 'capitalize',
              fontSize: '16px',
              lineHeight: '19px'
            }}>
              Назад
            </p>
          </Button>
          <Typography sx={{
            fontSize: '38px',
            fontWeight: '600',
            lineHeight: '42px',
            color: '#434343',
            mb: '20px',
            mx: '1px',
          }}>Добавление нового объекта</Typography>
        </Box>
        <Box sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          justifyContent: 'center',
        }}>
          <Button
            sx={{
              mt: '8px',
              height: "60px",
              width: '170px',
              backgroundColor: '#007AFF',
              fontWeight: '400',
              fontSize: "18px",
              lineHeight: '22px',
              borderRadius: "15px",
              boxShadow: 0,
              textTransform: 'lowercase',
            }} variant="contained" type="submit">
            <p style={{ textTransform: 'capitalize', fontSize: '16px', lineHeight: '19px' }}>
              Сохранить
            </p>
          </Button>
        </Box>
      </Stack>
      {/* Name Section */}
      <input
        style={{
          width: '640px',
          height: '56px',
          fontSize: '46px',
          lineHeight: '56px',
          backgroundColor: 'transparent',
          top: '10px',
          color: '#979797',
          margin: '20px 0px 0px 0px',
          border: 'none',
          outline: 'none'
        }}
        required
        name="club-name"
        placeholder="Введите название объекта"
        autoFocus
        onChange={(e: any) => {
          setCreateClubData(prevState => ({
            ...prevState,
            name: e.target.value,
          }))
        }}
      />
      {/* Photo Section */}
      <Stack
        className="Big-Blur"
        direction="row"
        justifyContent="center"
        alignItems="center"
        spacing={2}
        sx={{
          width: '100%',
          height: '480px',
          py: '20px',
          px: '20px',
        }}>
        {!fileSelected ? (
          <UploadImage />
        ) : (
          <>
            <img alt="not-found"
              style={{
                borderRadius: '15px',
                width: '493px',
                height: '432px',
              }}
              src={URL.createObjectURL(fileSelected)} />
            <Tooltip title="Delete" placement="bottom-end">
              <IconButton onClick={() => setFileSelected(null)}>
                <DeleteIcon />
              </IconButton>
            </Tooltip>
          </>
        )
        }
        <Stack>
          <Typography sx={{
            fontWeight: '600',
            fontSize: '36px',
            lineHeight: '48px',
            color: '#434343',
          }}>Фотографий пока нет</Typography>
          <Typography sx={{
            fontSize: '24px',
            lineHeight: '29px',
            color: '#434343',
          }}>Добавьте фотографии огранизации</Typography>
          <label htmlFor="upload-image"
            style={{
              cursor: 'pointer',
              width: '60%',
              backgroundColor: '#007AFF',
              height: '42px',
              borderRadius: '15px',
              fontSize: '18px',
              color: '#fff',
              padding: '10px 10px',
              display: 'flex',
              alignItems: 'center',
              justifyContent: 'center',
              marginTop: '30px',
            }}
          >Добавить Фото</label>
          <input
            required
            id="upload-image"
            accept="image/*"
            type="file"
            hidden
            onChange={handleImageChange}
          />
        </Stack>
      </Stack>

      {/* Price Section */}
      <Stack
        className="Big-Blur"
        direction="row"
        alignItems="center"
        justifyContent="center"
        sx={{
          width: '100%',
          height: '160px',
          py: '20px',
          px: '20px',
        }}>
        <Stack sx={{
          width: '100%'
        }}>
          <Typography
            sx={{
              fontSize: '46px',
              fontWeight: '600',
              lineHeight: '42px',
              color: '#434343',
              mb: '10px',
              px: '10px',
            }}>
            Цена за минуту
          </Typography>
          <FormControl
            fullWidth
            sx={{
              m: 1
            }}>
            <input
              id="price"
              name="price"
              required
              type="number"
              style={{
                padding: '10px 20px',
                width: "95.5%",
                height: '65px',
                backgroundColor: '#f7f7f7',
                borderRadius: '15px',
                outline: 'none',
                border: 'none',
                fontSize: '18px',
                lineHeight: '22px',
              }}
              onChange={(e: any) => {
                setCreateClubData(prevState => ({
                  ...prevState,
                  averagePrice: parseInt(e.target.value),
                }))
              }}
            />
          </FormControl>
        </Stack>
      </Stack>

      {/* Type Section */}
      <Stack
        className="Big-Blur"
        direction="row"
        alignItems="center"
        spacing={4}
        sx={{
          my: '20px',
          width: '100%',
          minHeight: '160px',
          py: '20px',
          px: '20px',
        }}>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            width: '100%'
          }}>
          <Typography
            sx={{
              fontSize: '46px',
              fontWeight: '600',
              lineHeight: '42px',
              color: '#434343',
              my: '20px',
              px: '10px',
            }}
          >
            Вид заведения
          </Typography>
          <Stack
            justifyContent="center"
            alignItems="start">
            <Stack
              alignItems="start"
              justifyContent="center"
              sx={{
                padding: '10px 20px',
                width: "95.5%",
                height: '65px',
                backgroundColor: '#f7f7f7',
                borderRadius: '15px',
                outline: 'none',
                border: 'none',
                fontSize: '18px',
                lineHeight: '22px',
              }}
              onClick={() => setOpenTypes(!openTypes)}
            >
              {institutionTypes?.find(institutionType => institutionType?.keyName === createClubData.type)?.locale['ru' || 'en']}
            </Stack>
            {openTypes ? (
              <Stack
                direction="column"
                alignItems="start"
                justifyContent="center"
                className="Big-Blur"
                spacing={2}
                sx={{
                  px: '20px',
                  py: '10px',
                  zIndex: 2,
                  backgroundColor: '#f7f7f7',
                }}
              >
                {institutionTypes?.map((institutionType: InstitutionTypes, index: number) => (
                  <Typography
                    key={index}
                    sx={{
                      my: '5px',
                      color: '#434343',
                      fontSize: '18px',
                      lineHeight: '22px',
                    }}
                    onClick={() => setCreateClubData(prevState => ({
                      ...prevState,
                      type: institutionType.keyName,
                    }))}
                  >
                    {institutionType?.locale['ru' || 'en']}
                  </Typography>
                ))}
              </Stack>
            ) : (
              <></>
            )}
          </Stack>
        </Box>
      </Stack>
      {/* Description Section */}
      <Stack
        className="Big-Blur"
        direction="row"
        alignItems="center"
        spacing={3}
        sx={{
          my: '20px',
          width: '100%',
          height: '288px',
          py: '20px',
          px: '20px',
        }}>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            width: '100%',
            height: '100%',
          }}>
          <Typography
            sx={{
              fontSize: '46px',
              fontWeight: '600',
              lineHeight: '42px',
              color: '#434343',
              my: '20px',
              px: '10px',
            }}>
            Описание
          </Typography>
          <textarea
            id="description"
            name="description"
            required
            style={{
              margin: '20px 10px',
              padding: '20px 30px',
              width: "90%",
              height: '145px',
              backgroundColor: '#f7f7f7',
              borderRadius: '15px',
              outline: 'none',
              border: 'none',
              fontSize: '18px',
              lineHeight: '22px',
            }}
            onChange={(e: any) => {
              setCreateClubData(prevState => ({
                ...prevState,
                description: e.target.value,
              }))
            }}
          />
        </Box>
        <Box>
          <DescriptionSVG />
        </Box>
      </Stack>
      
      {/* Address */}
      <Stack
        direction="row"
        alignItems="center"
        className="Big-Blur"
        spacing={4}
        sx={{
          my: '20px',
          width: '100%',
          height: '160px',
          py: '20px',
          px: '20px',
        }}>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            width: '100%'
          }}>
          <Typography
            sx={{
              fontSize: '46px',
              fontWeight: '600',
              lineHeight: '42px',
              color: '#434343',
              my: '20px',
              px: '10px',
            }}
          >
            Адрес
          </Typography>
          <input
            id="address"
            name="address"
            required
            style={{
              padding: '10px 20px',
              width: "95.5%",
              height: '65px',
              backgroundColor: '#f7f7f7',
              borderRadius: '15px',
              outline: 'none',
              border: 'none',
              fontSize: '18px',
              lineHeight: '22px',
            }}
            onChange={(e: any) => {
              setCreateClubData(prevState => ({
                ...prevState,
                address: e.target.value,
              }))
            }}
          />
        </Box>
        <LocationSVG />
      </Stack>
      {/* City */}
      <Stack
        className="Big-Blur"
        direction="row"
        alignItems="center"
        spacing={4}
        sx={{
          my: '20px',
          width: '100%',
          minHeight: '160px',
          py: '20px',
          px: '20px',
        }}>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            width: '100%'
          }}>
          <Typography
            sx={{
              fontSize: '46px',
              fontWeight: '600',
              lineHeight: '42px',
              color: '#434343',
              my: '20px',
              px: '10px',
            }}
          >
            Город
          </Typography>
          <Stack
            justifyContent="center"
            alignItems="start">
            <Stack
              alignItems="start"
              justifyContent="center"
              sx={{
                padding: '10px 20px',
                width: "95.5%",
                height: '65px',
                backgroundColor: '#f7f7f7',
                borderRadius: '15px',
                outline: 'none',
                border: 'none',
                fontSize: '18px',
                lineHeight: '22px',
              }}
              onClick={() => setOpenAddress(!openAddress)}
            >
              {createClubData.city}
            </Stack>
            {openAddress ? (
              <Stack
                direction="column"
                alignItems="start"
                justifyContent="center"
                className="Big-Blur"
                spacing={2}
                sx={{
                  px: '20px',
                  py: '10px',
                  zIndex: 2,
                  backgroundColor: '#f7f7f7',
                }}
              >
                <Typography
                  sx={{
                    my: '5px',
                    color: '#434343',
                    fontSize: '18px',
                    lineHeight: '22px',
                    cursor: 'pointer'
                  }}
                  onClick={() => {
                    setOpenAddress(false)
                    setCreateClubData(prevState => ({
                      ...prevState,
                      city: 'Алматы'
                    }))
                  }}
                >
                  Алматы
                </Typography>
                <Typography
                  sx={{
                    my: '5px',
                    color: '#434343',
                    fontSize: '18px',
                    lineHeight: '22px',
                    cursor: 'pointer'
                  }}
                  onClick={() => {
                    setOpenAddress(false)
                    setCreateClubData(prevState => ({
                      ...prevState,
                      city: 'Нурсултан'
                    }))
                  }}
                >
                  Нурсултан
                </Typography>
              </Stack>
            ) : (
              <></>
            )}
          </Stack>
        </Box>
      </Stack>
      {/* Working hours */}
      <Stack
        className="Big-Blur"
        direction="column"
        alignItems="start"
        spacing={4}
        sx={{
          my: '20px',
          width: '100%',
          py: '20px',
          px: '20px',
        }}>
        <Typography
          sx={{
            fontWeight: '600',
            fontSize: '28px',
          }}>
          График Работы
        </Typography>
        <Stack
          direction="row"
          alignItems="start"
          justifyContent="space-evenly"
          sx={{ width: '100%' }}>
          {/* Workdays Times Selection */}
          <Stack
            direction="column"
            alignItems="center"
            justifyContent="center"
            sx={{ width: '100%' }}
          >
            <Stack
              direction="row"
              alignItems="center"
              justifyContent="start"
              className="flex-row a-c j-c"
            >
              <input type="checkbox" defaultChecked={false} onChange={() => setWorkTimesActive(!workTimesActive)} />
              <Typography sx={{
                fontWeight: '600',
                fontSize: '24px',
                lineHeight: '29px',
                color: '#434343',
              }}>
                Все будние дни
              </Typography>
              <WorkTimesContainer
                disabled={!workTimesActive}
                time={selectTime.startTime}
                selectTime={(time) => {
                  setSelectedTime(prevState => ({
                    ...prevState,
                    startTime: time
                  }))
                  handleSelectTimes(7)
                }}
              />
              {'\u2014'}
              <WorkTimesContainer
                disabled={!workTimesActive}
                time={selectTime.startTime}
                selectTime={(time) => {
                  setSelectedTime(prevState => ({
                    ...prevState,
                    endTime: time
                  }))
                  handleSelectTimes(7)
                }}
              />
            </Stack>
            {/* Weekdays Time Selection */}
            <Stack
              direction="row"
              alignItems="center"
              justifyContent="center"
              sx={{
                width: '100%',
                height: '100%',
              }}
            >
              <Stack
                direction="column"
                alignItems="start"
                justifyContent="center">
                {["Понедельник", "Вторник", "Среда", "Четверг", "Пятница"].map((day: string, index: number) => (
                  <Stack
                    key={index}
                    direction="row"
                    alignItems="center"
                    justifyContent="start">
                    <WorkTimesContainer
                      disabled={workTimesActive}
                      time={selectTime.startTime}
                      selectTime={(time) => {
                        setSelectedTime(prevState => ({
                          ...prevState,
                          startTime: time
                        }))
                        handleSelectTimes(index)
                      }}
                    />
                    {'\u2014'}
                    <WorkTimesContainer
                      disabled={workTimesActive}
                      time={selectTime.endTime}
                      selectTime={(time) => {
                        setSelectedTime(prevState => ({
                          ...prevState,
                          endTime: time
                        }))
                        handleSelectTimes(index)
                      }}
                    />
                    <Typography sx={{
                      fontWeight: '400',
                      fontSize: '24px',
                      lineHeight: '29px',
                      color: '#434343',
                    }}>
                      {day}
                    </Typography>
                  </Stack>
                ))}
              </Stack>
            </Stack>
          </Stack>
          {/* Weekends Times Selection */}
          <Stack
            direction="column"
            alignItems="center"
            justifyContent="center"
            sx={{ width: '100%' }}
          >
            <Stack
              direction="row"
              alignItems="center"
              justifyContent="center">
              <input type="checkbox" defaultChecked={false} onChange={() => setWeekendWorkTimesActive(!weekendWorkTimesActive)} />
              <Typography sx={{
                fontWeight: '600',
                fontSize: '24px',
                lineHeight: '29px',
                color: '#434343',
              }}>
                Все выходные
              </Typography>
              <WorkTimesContainer
                disabled={!weekendWorkTimesActive}
                time={selectTime.startTime}
                selectTime={(time) => {
                  setSelectedTime(prevState => ({
                    ...prevState,
                    startTime: time
                  }))
                  handleSelectTimes(8)
                }}
              />
              {'\u2014'}
              <WorkTimesContainer
                disabled={!weekendWorkTimesActive}
                time={selectTime.startTime}
                selectTime={(time) => {
                  setSelectedTime(prevState => ({
                    ...prevState,
                    endTime: time
                  }))
                  handleSelectTimes(8)
                }}
              />
            </Stack>
            {/* Weekdays Time Selection */}
            <Stack
              direction="row"
              alignItems="center"
              justifyContent="center">
              <Stack
                direction="column"
                alignItems="start"
                justifyContent="center">
                {["Суббота", "Воскресенье"].map((day: string, index: number) => (
                  <Stack
                    direction="row"
                    alignItems="center"
                    justifyContent="center">

                    <WorkTimesContainer
                      disabled={weekendWorkTimesActive}
                      time={selectTime.startTime}
                      selectTime={(time) => {
                        setSelectedTime(prevState => ({
                          ...prevState,
                          startTime: time
                        }))
                        handleSelectTimes(index + 5)
                      }}
                    />
                    {'\u2014'}
                    <WorkTimesContainer
                      disabled={weekendWorkTimesActive}
                      time={selectTime.endTime}
                      selectTime={(time) => {
                        setSelectedTime(prevState => ({
                          ...prevState,
                          endTime: time
                        }))
                        handleSelectTimes(index + 5)
                      }}
                    />
                    <Typography sx={{
                      fontWeight: '400',
                      fontSize: '24px',
                      lineHeight: '29px',
                      color: '#434343',
                    }}>
                      {day}
                    </Typography>
                  </Stack>
                ))}
              </Stack>
            </Stack>
          </Stack>
        </Stack>
      </Stack>
      {/* Video */}
      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        spacing={2}
        className="Big-Blur"
        sx={{
          width: '100%',
          minHeight: '160px',
          py: '10px',
          px: '20px',
        }}>
        <Stack
          sx={{
            width: '90%',
          }}>
          <Typography
            sx={{
              fontSize: '46px',
              fontWeight: '600',
              lineHeight: '42px',
              color: '#434343',
              mb: '10px',
            }}>
            Видео
          </Typography>
          <FormControl sx={{ width: '100%' }}>
            <input
              id="address"
              name="address"
              style={{
                padding: '10px 20px',
                width: "95.5%",
                height: '65px',
                backgroundColor: '#f7f7f7',
                borderRadius: '15px',
                outline: 'none',
                border: 'none',
                fontSize: '18px',
                lineHeight: '22px',
              }}
              placeholder={"Ссылка на ролик с YouTube"}
              onChange={(e: any) => {
                setCreateClubData(prevState => ({
                  ...prevState,
                  videoURL: e.target.value,
                }))
              }}
            />
          </FormControl>
        </Stack>
        <VideoUploadSVG />
      </Stack>

      {/* Certifocates */}
      {/* <Stack></Stack> */}

      {/* Additional */}
      {/* <AdditionalClubPaper /> */}
      <Stack
        className="Big-Blur flex-col"
        spacing={4}
        sx={{
          my: '20px',
          width: '100%',
          py: '10px',
          px: '20px',
        }}>
        <Typography
          sx={{
            fontSize: '46px',
            fontWeight: '600',
            lineHeight: '42px',
            color: '#434343'
          }}>
          Дополнительно
        </Typography>
          <Stack
          className="flex-col"
          divider={
            <Divider
              orientation="horizontal" />
          }
          spacing={2}
          sx={{
            width: '100%'
          }}
        >
          {tags?.map((tag: Tags, index: number) => (
            <Stack
              key={index}
              className="flex-row a-c j-s_b"
              spacing={2}
              sx={{
                width: '100%'
              }}>
              <Typography
                sx={{
                  fontWeight: '400',
                  fontSize: '14px'
                }}>
                {tag.name}
              </Typography>
              <label className="switch">
                <input className="checkbox" defaultChecked={false} type="checkbox" onChange={(event: any) => {
                  console.log(index, event.target.checked)
                  handleTags(event.target.checked, index);
                }}/>
                <span className="slider round"></span>
              </label>
            </Stack>
          ))}
        </Stack>
      </Stack>

      {/* Contacts */}
      <Stack className="flex-col a-s j-c" sx={{ width: 'calc(100% + 40px)', px: '10px 20px', }}>
        <Typography
          sx={{
            fontSize: '46px',
            fontWeight: '600',
            lineHeight: '42px',
            color: '#434343',
            mb: '10px',
          }}>
          Контакты
        </Typography>
        <Stack className="flex-row a-c j-s_b" sx={{ width: '100%' }}>
          <ContactCard
            name="phone"
            icon={<PhoneIcon sx={{ width: '60px', height: '60px' }} />}
            type="phone"
            onChange={(value: string) => {
              setCreateClubData(prevState => ({
                ...prevState,
                phoneNumber: value,
              }))
            }}
          />
          <ContactCard
            name="instagram"
            icon={<InstagramIcon />}
            type="instagram"
            onChange={(value: string) => {
              setCreateClubData(prevState => ({
                ...prevState,
                instagram: value,
              }))
            }}
          />
        </Stack>
        <Stack className="flex-row a-c j-s_b" sx={{ width: '100%' }}>
          <ContactCard
            name="whatsapp"
            icon={<WhatsAppIcon />}
            type="whatsapp"
            onChange={(value: string) => {
              setCreateClubData(prevState => ({
                ...prevState,
                whatsApp: value,
              }))
            }}
          />
          <ContactCard
            name="telegram"
            icon={<TelegramIcon />}
            type="telegram"
            onChange={(value: string) => {
              setCreateClubData(prevState => ({
                ...prevState,
                telegram: value,
              }))
            }}
          />
        </Stack>
      </Stack>
    </Stack>
  );
};
interface CantactCardProps {
  icon: React.ReactElement;
  name:'phone' | 'instagram' | 'whatsapp' | 'telegram';
  type: 'phone' | 'instagram' | 'whatsapp' | 'telegram';
  onChange: (value: string) => void;
}

const ContactCard = ({ icon, type, name, onChange }: CantactCardProps) => {
  const inputPrefex = [
    {
      type: 'phone',
      header: 'Добавить номер телефона',
      text: 'Номер телефона',
      prefex: '+7',
    },
    {
      type: 'instagram',
      header: 'Добавить профиль в Instagram',
      text: 'Intsagram',
      prefex: 'instagram.com/',
    },
    {
      type: 'whatsapp',
      header: 'Добавить номeр WhatsApp',
      text: 'WhatsApp',
      prefex: 'wa.me/',
    },
    {
      type: 'telegram',
      header: 'Добавить Telegram',
      text: 'Telegram',
      prefex: 'tg.me/',
    },
  ];
  const items = inputPrefex.find(input => input.type === type);
  return (
    <Stack
      className="Big-Blur flex-col a-s j-c"
      sx={{
        width: '100%',
        height: '240px',
        p: '20px',
        m: '20px',
      }}
    >
      <Stack
        className="flex-col a-s j-s"
        spacing={2}
        sx={{ width: '100%', }}
      >
        <Stack
          className="flex-col a-s j-c"
          spacing={2}
        >
          <Box sx={{
            width: '60px',
            height: '60px',
          }}>
            {icon}
          </Box>
          <Typography sx={{
            fontSize: '28px',
            fontWeight: '500',
            lineHeight: '34px',
            color: '#434343'
          }}>{items?.header}</Typography>
        </Stack>
        <Stack className="flex-row a-c j-s_b" spacing={2} sx={{ width: '100%' }}>
          <FormControl sx={{
            width: '100%',
            backgroundColor: '#F7F7F7',
            borderRadius: '15px',
          }} >
            <Stack
              className="flex-row a-c j-c"
              sx={{
                py: '10px',
                px: '20px',
                backgroundColor: '#f7f7f7',
                width: '90%',
                height: '45px',
                borderRadius: '15px',
            }}>
              <Typography sx={{
                fontSize: '18px',
                lineHeight: '22px',
                color: ' #323232',
              }}>
              {items?.prefex}</Typography>
              <input 
                id="contacts"
                name="contacts"
              style={{
                width: "95.5%",
                backgroundColor: '#f7f7f7',
                borderRadius: '15px',
                outline: 'none',
                border: 'none',
                fontSize: '18px',
                lineHeight: '22px',
              }}
                onChange={(e: any) => {
                  onChange(e.target.value);
                }
                }
              />
            </Stack>
          </FormControl>
          {/* <IconButton sx={{ width: '60px', height: '60px', backgroundColor: '#CCCCCC', color: '#fff' }}>
            <CircleActionSVG />
          </IconButton> */}
        </Stack>
      </Stack>
    </Stack>
  )
};
