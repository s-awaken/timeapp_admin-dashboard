import React from 'react';
import { emitCustomEvent } from 'react-custom-events';
import dayjs from 'dayjs';
import { Box, Stack, Typography, Avatar, Button, TextField, CircularProgress } from '@mui/material';
import ArrowBackRoundedIcon from '@mui/icons-material/ArrowBackRounded';
import AttachFileIcon from '@mui/icons-material/AttachFile';

import { useQuery, useSubscription, useMutation} from '@apollo/client';

import { CHAT } from '../../graphql/subscription/chat';
import { GET_PREV_MESSAGES } from '../../graphql/query/getPrevMessages';
import { LEAVE_MESSAGE } from '../../graphql/mutations/leaveMessage';

import useLocalStorage from '../../hooks/useLocalStorage';


import { Chat, Message, User } from '../../interfaces/chat';


interface ChatPageProps {
  chat: Chat;
  close: () => void;
  user: User
}

const ChatWindow = ({ chat, close, user }: ChatPageProps) => {
  const [store] = useLocalStorage();
  const [text, setText] = React.useState('');
  const messages: Message[] = []
  const { data: dataMessages, loading } = useQuery(GET_PREV_MESSAGES,
    {
      context: {
        headers: {
          Authorization: store.getItem('JWT'),
        },
      },
      variables: {
        "chatId": chat?._id,
        "lastDate": "",
      },
      onError: (error) => {
        emitCustomEvent('error', error)
      },
    }
  );
  const {data: subData, loading: subLoading , error } = useSubscription(CHAT, {
    context: {
      headers: {
        Authorization: store.getItem('JWT'),
      },
    },
    variables: {
      "chatId": chat?._id,
    },
  });
  const [leaveMessage, { data: msgData, loading: msgLoad, error: msgError }] = useMutation(LEAVE_MESSAGE, {
    context: {
      headers: {
        Authorization: store.getItem('JWT'),
      },
    },
    variables: {
      "chatId": chat?._id,
      "text": text,
    },
    onCompleted: (data) => {
      setText('')
    }
  });
  React.useEffect(() => {
    messages.push(msgData?.leaveMessage);
  }, [msgData?.leaveMessage])
  if (error || msgError) emitCustomEvent('error', error || msgError);

  if (loading || subLoading) return (
    <CircularProgress sx={{
    alignSelf: 'center',
    justifySelf: 'center',
    my: 'auto',
    }} />)
  messages.push(...dataMessages.getPreviousMessages)

  return (
    <Stack direction="column" alignItems="center" spacing={2} sx={{ width: '100%', px: '20px', height: '800px'}}>
      {/* Header Section*/}
      <Stack direction="row"
        justifyContent="space-between"
        alignItems="center"
        spacing={4}
        sx={{
          width: '100%',
          pt: '20px',
          mx: '20px',
          height: '70px',
          boxShadow: 2,
        }}>
        <Button
          color="primary"
          variant="text"
          startIcon={<ArrowBackRoundedIcon />}
          onClick={() => close()}
        >Назад</Button>
        <Typography variant="h4">{user?.fullName}</Typography>
        <Avatar src={user?.photoURL?.XL} sx={{ width: '40px', height: '40px' }} />
      </Stack>
      {/* Messages View */}
      <Stack direction="column" sx={{ backgroundColor: '#f9f9f9', width: '100%', overflow: 'scroll', minHeight: '650px'}}>
        {/* messages Shit */}
        {messages.map((message: Message, index: number) => <MessageBox message={message}/>)}
      </Stack>
      {/* Send Messages Section */}
      <Stack
        direction="row"
        justifyContent="space-between"
        alignItems="center"
        sx={{
          height: '80px',
          width: '100%',
        }}
      >
        <AttachFileIcon sx={{transform: 'rotate(45deg)',}}/>
        <TextField
          sx={{
            width: '80%',
          }}
          value={text}
          onChange={(event) => setText(event.target.value)} />
        <Button
          variant="contained"
          color="primary"
          onClick={() => leaveMessage()}>
          отправить</Button>
      </Stack>
    </Stack>
  )
};

export default ChatWindow;


interface MessageProps {
  message: Message;
}
const MessageBox = ({ message }: MessageProps) => {
  const M = () => {
    return (
      <Box sx={{
        my: '10px',
        mx: '10px',
        borderRadius: '15px',
        color: message?.user?.__typename !== 'Owner' ? '#979797' : '#fff',
        backgroundColor: message?.user?.__typename !== 'Owner' ? '#fff' : '#62B4FF',
        minWidth: '50%',
        minHeight: '60px',
        alignItems: 'center',
        justifyContent: 'center',
      }}>
        <Typography>
          {message?.text}
        </Typography>
      </Box>
    )
  }

  return message?.user?.__typename !== 'Owner' ? (
      <Stack direction="row" justifyContent="space-between" alignItems="center">
        <M/>
        <div>{ dayjs(message.dateSent).format('DD.MM.YY')}</div>
      </Stack>
  ) : (
    <Stack direction="row" justifyContent="space-between" alignItems="center">
      <div>{dayjs(message.dateSent).format('DD.MM.YY')}</div>
      <M />
    </Stack>
  )  
}