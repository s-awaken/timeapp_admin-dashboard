import React from 'react';
import CustomSelect from '../components/CustomSelect';
import SessionCard from '../components/SessionCard';
import ClientPage from './MainModal/ClientPage'
import dayjs from 'dayjs';
import { emitCustomEvent } from 'react-custom-events';
import { ReactComponent as BarChartSVG } from '../images/barChart.svg';
import { ReactComponent as ThreePeopleSVG } from '../images/three-people.svg';

import { useQuery } from '@apollo/client';
import { VISITATIONS_OF_CLIENT } from '../graphql/query/visitationsOfClient';
import { GET_VISITATIONS_BY_PERIODS_OF_TIME } from '../graphql/query/getHistory';
import { GET_MAX_PAGES } from '../graphql/query/getMaxPages';
import { GET_STATS } from '../graphql/query/getStats';
import { useInstitution } from '../utils/recoil/instituition.hook';
import useLocalStorage from '../hooks/useLocalStorage';
import { Session } from '../interfaces/session';
import { Institution } from '../interfaces/institution';
import {Client} from '../interfaces/client';
import '../styles/Main.scss';

const Main: React.FC = () => {
  const [sessionType, setSessionType] = React.useState<number>(0);
  const [store] = useLocalStorage();
  const [institutions, { setInstitution }] = useInstitution();
  const [dates, setDates] = React.useState({
    firstDate: new Date(new Date().setUTCHours(0, 0, 0, 0)).toISOString(),
    secondDate: new Date(
      new Date().setUTCHours(23, 59, 59, 999)
    ).toISOString()
  })
  const [openClient, setOpenClient] = React.useState<boolean>(false);
  const [client, setClient] = React.useState<Client>();
  const [selectedInstituition, setSelectedInstitution] = React.useState<Institution>();
  const [page, setPage] = React.useState(1);
  
  const {
    data,
    loading,
    error
  } = useQuery(VISITATIONS_OF_CLIENT, {
    context: {
      headers: {
        Authorization: store.getItem('JWT')
      }
    },
    pollInterval: 5000,
    variables: {
      "institutionID": selectedInstituition?._id || null,
      "page": page,
      "sortBy": "Date",
      "isActive": sessionType === 0,
    }
  });
  const {
    data: endedSessionsToday,
    loading: todayVisitsLoading,
    error: todayVisitsError
  } = useQuery(GET_VISITATIONS_BY_PERIODS_OF_TIME, {
    context: {
      headers: {
        Authorization: store.getItem('JWT')
      }
    },
    pollInterval: 5000,
    variables: {
      firstDate: dates.firstDate,
      secondDate: dates.secondDate,
      page: 1,
    },
  })
  const {
    data: maxPageData,
    loading: maxPageLoading,
    error: maxPageError,
  } = useQuery(GET_MAX_PAGES, {
    context: {
      headers: {
        Authorization: store?.getItem('JWT')
      }
    },
    variables: {
      "institutionIds": institutions.constructor === Array ?
        (institutions?.map(institution => institution?._id)[0] === "" ?
          undefined :
          institutions?.map(institution => institution?._id)
        ) : (
          institutions[0]?._id
        )
      ,
      "currentPage": page
    }
  })
  const varsForStats = {
    firstDate: dates.firstDate,
    secondDate: dates.secondDate,
    "separateBy": "Day",
    "institutionIds": institutions.constructor === Array ?
      (institutions?.map(institution => institution?._id)[0] === "" ?
        undefined :
        institutions?.map(institution => institution?._id)
      ) : (
        institutions[0]?._id
      )
    ,
  };
  const {
    data: todayStatsData,
    loading: todayStatsLoading,
    error: todayStatsError,
  } = useQuery(GET_STATS, {
    context: {
      headers: {
        Authorization: store?.getItem('JWT')
      }
    },
    pollInterval: 5000,
    variables: {
      ...varsForStats
    }
  })

  if (loading || maxPageLoading) {
    emitCustomEvent('loading', loading || maxPageLoading)
  }
  emitCustomEvent('loading', false)
  if (error || maxPageError) emitCustomEvent('error', error || maxPageError)
  
  const activeSessions: Session[] = data?.getVisitationsForAdminPanel;
  const sessions: Session[] = endedSessionsToday?.getVisitationsByPeriodsOfTime;
  const pages: number = maxPageData?.getMaximumPageOfVisitationsForOwner;
  
  const handleOpenClient = (index: number) => {
    setClient(activeSessions[index]?.client);
    setOpenClient(true);
  }
  if (openClient && client) return <ClientPage client={client} close={() => setOpenClient(false)} />
  const earningsToday = todayStatsData?.getStatsByDates[0]?.earnings;
  const trainingsToday = todayStatsData?.getStatsByDates[0]?.numOfSessions;
  return (
    <div className="Main">
      {/* Header Section -- Start */}
      <div className="Header">
        <p className="Title">
          Сессии за сегодня {'\u2014'} {dayjs(new Date()).format('DD.MM.YYYY')}
        </p>
        <div>
          {
            institutions.constructor === Array && (
              <CustomSelect
                items={institutions.map(institution => institution.name)}
                selectItems={(item: Institution) => setSelectedInstitution(item)}
                selectedItem={selectedInstituition?.name}
              />
            )
          }
        </div>
      </div>
      <div className="Page">
        {/* Header Section -- End */}
        <div className="Container">
          {/* Post-Header -- Start */}
          <div className="Post-Header">
            {/* Tab Section -- Start */}
            <div className="Tabs">
              <div
                className={sessionType === 0 ? `Tab -active` : 'Tab'}
                onClick={() => setSessionType(0)}
              >
                <p >Активные</p>
              </div>
              <div
                className={sessionType === 1 ? `Tab -active` : 'Tab'}
                onClick={() => setSessionType(1)}
              >
                <p >Завершенные</p>
              </div>
            </div>
            {/* Tab Section -- End */}

            {/* Search Section -- Start */}
            <div className="Search">
              <div className="">
              </div>
              <p>Поиск</p>
            </div>
            {/* Search Section -- End */}
          </div>
          {/* Post-Header -- End */}
          
          {/* Sessions -- Start*/}
          <div className="Sessions">
            {sessionType === 0 ? activeSessions?.map((session: Session, index: number) => (
              <SessionCard key={index} session={session} setOpen={() => handleOpenClient(index)} />
            )
            ) :
              sessions?.map((session: Session, index: number) => (
                <SessionCard key={index} session={session} setOpen={() => handleOpenClient(index)} />
              )
              )}
          </div>
          {/* Sessions -- End*/}

        </div>
        {/* Info Papers -- Start */}
        <div className="Info">
          {/* Today Earning Section -- Start*/}
          <div className="Info-Paper">
            <BarChartSVG />
            <div className="Info">
              <p className="Money">{earningsToday || "0"} ₸</p>
              <p className="Description">За сегодня</p>
            </div>
          </div>
          {/* Today Earning Section -- End*/}

          {/* Today Visits Section -- Start*/}
          <div className="Info-Paper">
            <ThreePeopleSVG />
            <div className="Info">
              <p className="Money">{trainingsToday || "0"}</p>
              <p className="Description">Активных сессий</p>
            </div>
          </div>
          {/* Today Visits Section -- End*/}
        </div>
        {/* Info Papers -- End */}
      </div>
    </div>
  );
}


export default Main;