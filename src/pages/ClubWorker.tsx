import * as React from 'react';
import { Box, Typography, Stack, Checkbox, Divider, Switch, FormControl } from '@mui/material';
import TimePicker from 'react-time-picker';
import PhoneIcon from '@mui/icons-material/Phone';

import { ReactComponent as TelegramIcon } from '../images/Specs/telegram.svg';
import { ReactComponent as InstagramIcon } from '../images/Specs/instagram.svg';
import {ReactComponent as WhatsAppIcon} from '../images/Specs/whatsapp.svg'
import { ReactComponent as ArrowLeft } from '../images/arrowLeft.svg';
import { ReactComponent as ArrowRight } from '../images/arrowRight.svg';
import { ReactComponent as EditSVG } from '../images/edit.svg';
import { ReactComponent as StarSVG } from '../images/bigStar.svg';
import { ReactComponent as RatingSVG } from '../images/ratings.svg';
import { ReactComponent as DescriptionSVG } from '../images/description.svg';
import { ReactComponent as LocationSVG } from '../images/location.svg';
import { ReactComponent as VideoUploadSVG } from '../images/video_upload.svg';
import { ReactComponent as WorkingHoursSVG } from '../images/working_hours.svg';
import CustomTimeInput from '../components/CustomTimeInput';
import useLocalStorage from '../hooks/useLocalStorage';
import { useInstitution } from '../utils/recoil/instituition.hook';

import { PhotoURL } from '../interfaces/photo';

interface WorkDay {
  day: string;
  time: [string, string];
  rest: [string, string];
}


export default function ViewClubWorker() {
  const [institutions, { setInstitution }] = useInstitution();
  const { _id, location, avatarURL, averagePrice, name, rating, numOfWorkers, ratingCount, description, videoURL, contacts, galleryURLs } = institutions[0];

  const [store] = useLocalStorage();

  const [fileSelected, setFileSelected] = React.useState<PhotoURL>(avatarURL);
  const [galleryPhotoIndex, setGalleryPhotoIndex] = React.useState<number>(0);

  const handleGallery = (event: any, next: boolean): void => {
    event.preventDefault();
    if (!galleryURLs) return;
    const index = next ? galleryPhotoIndex + 1 : galleryPhotoIndex - 1;
    setGalleryPhotoIndex(index);
    setFileSelected(index === 0 ? avatarURL : galleryURLs[index]);
  }

  return (
    <Stack
      direction="column"
      alignItems="flex-start"
      justifyContent="flex-start"
      spacing={2}
      sx={{
        width: '100%',
      }}>
      {/* Header */}
      <Stack
        direction="row"
        justifyContent="space-between"
        sx={{
          width: '100%',
          py: '10px',
          px: '20px',
        }}>
        <Box sx={{
          display: 'flex',
          flexDirection: 'column',
          alignItems: 'flex-start',
          justifyContent: 'center',
        }}>
          <Typography sx={{
            fontWeight: '600',
            fontSize: '38px',
            lineHeight: '42px',
            color: '#434343'
          }}>{name}</Typography>
        </Box>
      </Stack>

      {/* Photo Section */}
      <Stack
        direction="column"
        alignItems="center"
        justifyContent="center"
        sx={{
          width: '100%',
          height: '586px',
          borderRadius: '15px',
          backgroundImage: `url(${fileSelected?.XL})`,
          py: '10px',
          px: '20px',
        }}
      >
        <Stack
        direction="row"
        alignItems="center"
        justifyContent="space-between"
        sx={{
          width: '100%',
          height: '576px',
        }}>
          <Stack
            direction="row"
            alignItems="start"
            justifyContent="start"
            sx={{
              width: '100%',
            }}
          >
            <button
              className="Chevron-Button"
              style={{
                display: 'flex',
                justifyContent: 'flex-start'
              }}
              disabled={!galleryURLs || galleryPhotoIndex === 0}
              onClick={(e) => handleGallery(e, false)}>
              <ArrowLeft />
            </button>
          </Stack>
          <Stack
            direction="row"
            alignItems="end"
            justifyContent="end"
            sx={{
              width: '100%',
              justifyContent: 'flex-end'
            }}
          >
            <button
              style={{
                display: 'flex',
                justifyContent: 'flex-end'
              }}
              className="Chevron-Button"
              disabled={!galleryURLs || galleryURLs?.length === galleryPhotoIndex}
              onClick={(e) => handleGallery(e, true)}>
              <ArrowRight />
            </button>
          </Stack>
        </Stack>
      </Stack>
      {/* Tabs Section */}
      <Stack
        direction="row"
        alignItems="center"
        spacing={6}
        sx={{
          width: '100%',
          my: '20px',
        }}>
        {/* --Price */}
        <Stack
          className="Big-Blur"
          direction="row"
          alignItems="center"
          justifyContent="center"
          spacing={2}
          sx={{
            width: '100%',
            height: '87px',
            px: '20px',
            mx: '20px 0',
            backgroundColor: '#fff',
          }}>
          <input
            disabled
            className="Input-Placeholder"
            type="numer"
            style={{
              fontWeight: 'normal',
              fontSize: '38px',
              lineHeight: '46px',
              color: '#434343',
              outline: 'none',
              backgroundColor: 'transparent',
              border: 'none',
              textAlign: 'center',
              width: '14%',
            }}
            placeholder={averagePrice.toString()}
          />
          <Typography sx={{
            width: '36%',
            fontWeight: 'normal',
            fontSize: '38px',
            lineHeight: '46px',
            color: '#434343',
          }}>₸/мин</Typography>
          <EditSVG
            style={{ cursor: 'pointer' }}
          />
        </Stack>
        {/* --Rating Score */}
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="center"
          spacing={2}
          sx={{
            width: '100%',
            height: '87px',
            px: '20px',
            mx: '20px',
            borderRadius: '15px',
            backgroundColor: '#f9f9f9',
          }}>
          <StarSVG />
          <Typography
            sx={{
              fontWeight: 'normal',
              fontSize: '38px',
              lineHeight: '46px',
              color: '#00DF31',
            }}
          >
            {rating}
          </Typography>
        </Stack>
        {/* --Ratings Count*/}
        <Stack
          direction="row"
          alignItems="center"
          justifyContent="center"
          spacing={2}
          sx={{
            width: '100%',
            height: '87px',
            px: '20px',
            mx: '0 20px',
            borderRadius: '15px',
            backgroundColor: '#f9f9f9',
          }}>
          <RatingSVG />
          <Typography
            sx={{
              fontWeight: 'normal',
              fontSize: '38px',
              lineHeight: '46px',
              color: '#434343',
            }}
          >
            {ratingCount}
          </Typography>
        </Stack>
      </Stack>

      {/* Description Section */}
      <Stack
        className="Big-Blur"
        direction="row"
        alignItems="center"
        spacing={3}
        sx={{
          my: '20px',
          width: '100%',
          height: '288px',
          py: '10px',
          px: '20px',
        }}>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            width: '100%',
            height: '100%',
          }}>
          <Typography
            sx={{
              fontSize: '46px',
              fontWeight: '600',
              lineHeight: '42px',
              color: '#434343',
              my: '10px',
              px: '10px',
            }}>
            Описание
          </Typography>
          <textarea
            disabled
            id="description"
            name="description"
            style={{
              margin: '20px 10px',
              padding: '20px 30px',
              width: "90%",
              height: '145px',
              backgroundColor: '#f7f7f7',
              borderRadius: '15px',
              outline: 'none',
              border: 'none',
              fontSize: '18px',
              lineHeight: '22px',
            }}
            placeholder={description}
          />
        </Box>
        <Box sx={{height: '80px', width: '80px'}}>
          <DescriptionSVG />
        </Box>
      </Stack>
      
      {/* Address */}
      <Stack
        direction="row"
        alignItems="center"
        spacing={4}
        className="Big-Blur"
        sx={{
          my: '20px',
          width: '100%',
          height: '220px',
          py: '10px',
          px: '20px',
        }}>
        <Box
          sx={{
            display: 'flex',
            flexDirection: 'column',
            width: '100%'
          }}>
         <Typography
            sx={{
              fontSize: '46px',
              fontWeight: '600',
              lineHeight: '42px',
              color: '#434343',
              my: '20px',
              px: '10px',
            }}
          >
            Адрес
          </Typography>
            <input
            disabled
            style={{
              padding: '0 10px',
              marginBottom: '20px',
                fontSize: '28px',
                lineHeight: '33px',
                color: '#979797',
                outline: 'none',
                backgroundColor: 'transparent',
                border: 'none',
              }}
            autoFocus
            placeholder={location?.address}
          />
        </Box>
        <Box sx={{height: '80px', width: '80px'}}>
          <LocationSVG />
        </Box>
      </Stack>
      
      {/* Working hours */}
      {/* <WorkingHours/> */}
      
      {/* Video */}
      <Stack
        direction="column"
        justifyContent="space-between"
        alignItems="center"
        spacing={2}
        className="Big-Blur"
        sx={{
          width: '100%',
          minHeight: '120px',
          py: '10px',
          px: '20px',
        }}>
        <Stack
          direction="row"
          justifyContent="space-between"
          alignItems="center"
          spacing={2}
          sx={{
            width: '100%',
            height: '120px',
          }}
        >
          <Stack
            sx={{
              width: '90%',
            }}>
            <Typography
              sx={{
              fontSize: '46px',
              fontWeight: '600',
              lineHeight: '42px',
                color: '#434343',
              mb: '10px',
              }}>
              Видео
            </Typography>
            <FormControl
              sx={{
                width: '75%',
                height: '65px',
                backgroundColor: '#F9FBFF',
              }}>
           <input 
              id="video"
              name="video"
                style={{
                  padding: '10px 20px',
                  width: "95.5%",
                  height: '65px',
                  backgroundColor: '#F9FBFF',
                  borderRadius: '15px',
                  outline: 'none',
                  border: '1px solid #0F84F4',
                  fontSize: '18px',
                  lineHeight: '22px',
                }}
                disabled
              placeholder={videoURL}
            />
            </FormControl>
          </Stack>
          <Box sx={{height: '80px', width: '80px'}}>
            <VideoUploadSVG />
          </Box>
        </Stack>
        {videoURL ? (
          <Stack sx={{width: '100%',}}>
            <iframe
              title="video"
              width="100%"
              height="530px"
              src={`${videoURL?.replace("https://www.youtube.com/watch?v=",
              "https://www.youtube.com/embed/"
              )}`}
              style={{borderRadius: '15px',}}
            ></iframe>
          </Stack>
        ) : (
            <></>
        )}
      </Stack>

      {/* Certifocates */}
      {/* <Stack></Stack> */}

      {/* Additional */}
      {/* <AdditionalClubInfo /> */}

      {/* Contacts */}
      <Stack direction="column" spacing={4} justifyContent="center" alignItems="center" sx={{ width: '100%' }}>
        <Stack direction="row" spacing={4} justifyContent="center" alignItems="center" sx={{ width: '100%' }}>
          <ContactCard
            name="phone"
            icon={<PhoneIcon sx={{width: '60px', height: '60px'}}/>}
            type="phone"
            content={contacts?.phoneNumber}
          />
          <ContactCard
            name="instagram"
            icon={<InstagramIcon/>}
            type="instagram"
            content={contacts?.instagram}
            />
          </Stack>
        <Stack direction="row" spacing={4} justifyContent="center" alignItems="center" sx={{ width: '100%' }}>
          <ContactCard
            name="whatsapp"
            icon={<WhatsAppIcon/>}
            type="whatsapp"
            content={contacts?.whatsApp}
            />
          <ContactCard
            name="telegram"  
            icon={<TelegramIcon />}
            type="telegram"
            content={contacts?.phoneNumber}
            />
        </Stack>
      </Stack>
    </Stack>
  );
};
interface CantactCardProps {
  icon: React.ReactElement;
  name:'phone' | 'instagram' | 'whatsapp' | 'telegram';
  type: 'phone' | 'instagram' | 'whatsapp' | 'telegram';
  content: string;
}

const ContactCard = ({ icon, type, name, content }: CantactCardProps) => {
  const inputPrefex = [
    {
      type: 'phone',
      header: 'Основной номер телефона',
      text: 'Номер телефона',
      prefex: '+7',
    },
    {
      type: 'instagram',
      header: 'Профиль в Instagram',
      text: 'Intsagram',
      prefex: 'instagram.com/',
    },
    {
      type: 'whatsapp',
      header: 'Номер WhatsApp',
      text: 'WhatsApp',
      prefex: 'wa.me/',
    },
    {
      type: 'telegram',
      header: 'Профиль Telegram',
      text: 'Telegram',
      prefex: 'tg.me/',
    },
  ];
  const items = inputPrefex.find(input => input.type === type);
  return (
    <Stack
      direction="column"
      alignItems="start"
      justifyContent="center"
      className="Big-Blur"
      sx={{
        width: '100%',
        height: '240px',
        p: '20px',
      }}
    >
      <Stack
        direction="column"
        alignItems="start"
        justifyContent="start"
        spacing={2}
        sx={{ width: '100%' }}
      >
        <Stack
          direction="column"
          alignItems="start"
          justifyContent="center"
          spacing={2}
        >
          <Box sx={{
            width: '60px',
            height: '60px',
          }}>
            {icon}
          </Box>
          <Typography sx={{
            fontSize: '28px',
            fontWeight: '500',
            lineHeight: '34px',
            color: '#434343'
          }}>{items?.header}</Typography>
        </Stack>
        <Stack direction="row" spacing={2} justifyContent="space-between" alignItems="center" sx={{ width: '100%' }}>
          <FormControl sx={{
            width: '100%',
            backgroundColor: '#F7F7F7',
            borderRadius: '15px',
          }} >
            <Stack
              direction="row"
              alignItems="center"
              justifyContent="center"
              sx={{
                py: '10px',
                px: '20px',
                backgroundColor: '#f7f7f7',
                width: '90%',
                height: '45px',
                borderRadius: '15px',
            }}>
              <Typography sx={{
                fontSize: '18px',
                lineHeight: '22px',
                color: ' #323232',
              }}>
              {items?.prefex}</Typography>
              <input
                disabled
                id="contacts"
                name="contacts"
                style={{
                  width: "95.5%",
                  backgroundColor: '#f7f7f7',
                  borderRadius: '15px',
                  outline: 'none',
                  border: 'none',
                  fontSize: '18px',
                  lineHeight: '22px',
                }}
                value={content}
            />
            </Stack>
          </FormControl>
        </Stack>
      </Stack>
    </Stack>
  )
};

interface WorkingHoursProps {
  setWorkHours: (arg0: any) => void;
  workHours: any[];
}
const WorkingHours = ({setWorkHours, workHours}: WorkingHoursProps ) => {
  return (
    <Stack
      direction="column"
      alignItems="start"
      spacing={4}
      sx={{
        my: '20px',
        boxShadow: 2,
        borderRadius: "14px",
        width: '100%',
        height: '320px',
        py: '18px',
        px: '24px',
      }}>
      <Typography
        sx={{
          fontWeight: '600',
          fontSize: '28px',
          my: '6px',
        }}>
        График Работы
      </Typography>
      <Stack
        direction="row"
        spacing={2}
        justifyContent="space-between"
        alignItems="center"
        sx={{
          width: '100%'
        }}>
        {/* Right Side */}
        <Stack
          direction="row"
          spacing={2}
          justifyContent="center"
          alignItems="center">
          {/* WeekDays */}
          <Stack
            direction="row"
            spacing={2}
            justifyContent="center"
            alignItems="center">
            <Checkbox />
            <Typography
              sx={{
                fontWeight: '600',
                fontSize: '16px'
              }}>
              Все будни
            </Typography>
            <TimePicker
              disableClock
              format="hh:mm"
              value={workHours[0]?.time[0]}
              onChange={(newValue) => {
                setWorkHours((prevState:any) => ({
                  ...prevState,
                  time: newValue
                })
                )
              }}
            />
            <Typography>-</Typography>
            <CustomTimeInput time="22:00" />
          </Stack>
          {/* Weekends */}
          <Stack
            direction="row"
            spacing={2}
            justifyContent="center"
            alignItems="center">
            <Checkbox />
            <Typography sx={{
              fontWeight: '600',
              fontSize: '16px'
            }}>
              Все будни
            </Typography>
            <CustomTimeInput time="09:00" />
            <Typography>-</Typography>
            <CustomTimeInput time="22:00" />
          </Stack>
        </Stack>
        {/* Left Side */}
        <WorkingHoursSVG />
      </Stack>
    </Stack>
  );
};

const AdditionalClubInfo = () => {
  return (
    <Stack
      direction="column"
      alignItems="start"
      spacing={4}
      sx={{
        my: '20px',
        boxShadow: 2,
        borderRadius: "14px",
        width: '100%',
        py: '18px',
        px: '24px',
      }}>
      <Typography
        sx={{
          fontWeight: '600',
          fontSize: '16px'
        }}>
        Дополнительно
      </Typography>
      <Stack
        direction="column"
        divider={
          <Divider
            orientation="horizontal" />
        }
        spacing={2}
        sx={{
          width: '100%'
        }}
      >
        <Stack
          direction="row"
          spacing={2}
          justifyContent="space-between"
          alignItems="center"
          sx={{
            width: '100%'
          }}>
          <Typography
            sx={{
              fontWeight: '400',
              fontSize: '14px'
            }}>
            Оплата картой
          </Typography>
          <Switch />
        </Stack>
        <Stack
          direction="row"
          spacing={2}
          justifyContent="space-between"
          alignItems="center"
          sx={{
            width: '100%'
          }}>
          <Typography
            sx={{
              fontWeight: '400',
              fontSize: '14px'
            }}>
            WiFi
          </Typography>
          <Switch />
        </Stack>
        <Stack
          direction="row"
          spacing={2}
          justifyContent="space-between"
          alignItems="center"
          sx={{
            width: '100%'
          }}>
          <Typography
            sx={{
              fontWeight: '400',
              fontSize: '14px'
            }}>
            Платная Парковка
          </Typography>
          <Switch />
        </Stack>
        <Stack
          direction="row"
          spacing={2}
          justifyContent="space-between"
          alignItems="center"
          sx={{
            width: '100%'
          }}>
          <Typography
            sx={{
              fontWeight: '400',
              fontSize: '14px'
            }}>
            Бесплатная Паркока
          </Typography>
          <Switch />
        </Stack>
        <Stack
          direction="row"
          spacing={2}
          justifyContent="space-between"
          alignItems="center"
          sx={{
            width: '100%'
          }}>
          <Typography sx={{ fontWeight: '400', fontSize: '14px' }}>
            Бассейн
          </Typography>
          <Switch />
        </Stack>
        <Stack direction="row" spacing={2} justifyContent="space-between" alignItems="center" sx={{ width: '100%' }}>
          <Typography sx={{ fontWeight: '400', fontSize: '14px' }}>
            Русская Баня
          </Typography>
          <Switch />
        </Stack>
        <Stack direction="row" spacing={2} justifyContent="space-between" alignItems="center" sx={{ width: '100%' }}>
          <Typography sx={{ fontWeight: '400', fontSize: '14px' }}>
            Хамам
          </Typography>
          <Switch />
        </Stack>
      </Stack>

    </Stack>
  );
};