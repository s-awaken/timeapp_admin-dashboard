import React from 'react'
import { useMutation } from '@apollo/client';
import { emitCustomEvent } from 'react-custom-events';

import { EDIT_OWNER } from '../graphql/mutations/editOwner';
import { useOwner } from '../utils/recoil/owner.hook';
import useLocalStorage from '../hooks/useLocalStorage';
import '../styles/Settings.scss'
interface OwnerData {
  email: string;
  phone?: string;
  oldPassword: string;
  newPassword: string;
}
const SettingsPage = () => {
  const [owner, { setOwner }] = useOwner();
  const [store] = useLocalStorage();
  const [ownerUploadData, setOwnerUploadData] = React.useState<OwnerData>({
    email: "",
    phone: "",
    oldPassword: "",
    newPassword: "",
  })
  const [editOwner, { loading, error }] = useMutation(EDIT_OWNER, {
    context: {
      headers: {
        Authorization: store.getItem("JWT")
      }
    }
  });
  if (loading) emitCustomEvent('loading', loading);
  emitCustomEvent('loading', false);
  if (error) emitCustomEvent('req-error', error);
  const checkData = () => {
    return ownerUploadData.email.length > 0 && ownerUploadData.newPassword.length > 0 && ownerUploadData.oldPassword.length > 0;
  }
  const handleEditOwner = (event:  React.FormEvent<HTMLFormElement>) => {
    event.preventDefault();
    editOwner({
      variables: {
        email: ownerUploadData.email,
        oldPassword: ownerUploadData.oldPassword,
        newPassword: ownerUploadData.newPassword,
    }})
  }
  const logout = () => { 
    store.removeItem('JWT');
    store.removeItem("USER");
    window.location.reload();
  }

  return (
    <div className="Settings">
      <p className="Title">Настройки</p>
      {store.getItem("USER") === "Owner" ? (
        <div className="Form">
          <p className="Title-2">Главное</p>
          <input
            type="email"
            name="email"
            placeholder={ownerUploadData?.email}
            onChange={(e: any) => setOwnerUploadData(prevState => ({
              ...prevState,
              email: e.target.value,
            }))}
          />
          <input
            type="phone"
            name="phone"
            placeholder={ownerUploadData?.phone || "Телефон"}
            onChange={(e: any) => setOwnerUploadData(prevState => ({
              ...prevState,
              phone: e.target.value,
            }))}
          />
          <input
            type="password"
            name="oldPassword"
            placeholder="Старый пароль"
            onChange={(e: any) => setOwnerUploadData(prevState => ({
              ...prevState,
              oldPassword: e.target.value,
            }))}
          />
          <input
            type="password"
            name="newPassword"
            placeholder="Новый пароль"
            onChange={(e: any) => setOwnerUploadData(prevState => ({
              ...prevState,
              newPassword: e.target.value,
            }))}
          />
          <button
            type="submit"
            disabled={!checkData()}
            onClick={(e: any) => handleEditOwner(e)}
          >
            <p
              className="Cap"
              style={{
                fontSize: '16px',
                lineHeight: '19px'
              }}>
              Сохранить Изменения
            </p>
          </button>
        </div>
      ) : (<></>)}
      <div className="Form">
        <p className="Title-2">Компания</p>
        <input
          placeholder="БИН/ИИН"
          disabled />
        <input
          placeholder={owner?.nameOfTheOrganzation || "Название компании"}
          disabled />
        <input
          type="text"
          placeholder="Имя ответственного"
          disabled />
        <button disabled>
          <p className="Cap"
            style={{
              fontSize: '16px',
              lineHeight: '19px'
            }}>
            {"Изменить данные о компании"}
          </p>
        </button>
      </div>
      <button
        className="Logout-Button"  
        onClick={() => logout()}
      >
        <p className="Cap"
          style={{
            fontSize: '16px',
            lineHeight: '19px',
            color: '#F40F0F'
          }}>
          {"Выйти из профиля"}
        </p>
      </button>
    </div>
  );
};

export default SettingsPage;