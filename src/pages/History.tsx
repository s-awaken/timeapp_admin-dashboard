import React from 'react'
import dayjs from 'dayjs';
import CustomSelect from '../components/CustomSelect';
import { useQuery, useLazyQuery } from '@apollo/client';
import { emitCustomEvent } from 'react-custom-events';
import { Session } from '../interfaces/session';
import { Institution } from '../interfaces/institution';
import { GET_STATS } from '../graphql/query/getStats';
import { VISITATIONS_OF_CLIENT } from '../graphql/query/visitationsOfClient';
import useLocalStorage from '../hooks/useLocalStorage';
import { useInstitution } from '../utils/recoil/instituition.hook';
import { ReactComponent as AvPayClient } from '../images/threePeople.svg';
import { ReactComponent as AvTimeClient } from '../images/cashMoney.svg';
import { ReactComponent as ArrowForward } from '../images/arrowChevronForward.svg';
import { ReactComponent as ArrowBack } from '../images/arrowChevronBack.svg';
import avatar from '../images/emptyAvatar.png'
import "../styles/History.scss";

type Interval = 'date' | 'month' | 'year'
type AllowedStatsDateTypes = "Day" | "Month" | "Year"
interface Stat {
  earnings: number
  dates: {
    value: number;
    dateType: AllowedStatsDateTypes;
  }[];
  numOfSessions: number
}


const HistoryPage = () => {
  const [store] = useLocalStorage();
  const [institutions, { setInstitution }] = useInstitution();
  
  const [openClient, setOpenClient] = React.useState<boolean>(false);
  const [client, setClient] = React.useState()
  const [selectedInstituition, setSelectedInstitution] = React.useState<Institution>();
  const [stats, setStats] = React.useState<Stat[] | null>(null);
  const [interval, setInterval] = React.useState<Interval>("month");
  const [separateBy, setSeparateBy] = React.useState<AllowedStatsDateTypes>("Month");
  const [statPage, setStatPage] = React.useState<number>(0);

  const [dates, setDates] = React.useState({
    firstDate: dayjs().set('month', dayjs().get('month') - 4).toISOString(),
    secondDate: dayjs().toISOString()
  });
  React.useEffect(() => {
    handleStats(interval, statPage)
    getHistory()
  }, [interval, statPage])

  const {
    data: visitationsData,
    loading: visitationsLoading,
    error: visitationsError
  } = useQuery(VISITATIONS_OF_CLIENT, {
    context: {
      headers: {
        Authorization: store?.getItem('JWT'),
      },
    },
    variables: {
      "page": 1,
      "sortBy": 'Date',
      "isActive": false,
    },
  });

  const [getHistory,{
    loading: historyLoading,
    error: historyError,
  }] = useLazyQuery(GET_STATS, {
    context: {
      headers: {
        Authorization: store?.getItem('JWT'),
      },
    },
    variables: {
      firstDate: dates?.firstDate,
      secondDate: dates?.secondDate,
      "separateBy": separateBy,
      "institutionIds": institutions.constructor === Array ?
        (institutions?.map(institution => institution?._id)[0] === "" ?
          undefined :
          institutions?.map(institution => institution?._id)
        ) : (
          [institutions[0]?._id]
        )
    },
    onCompleted: (data) => {
      setStats(data.getStatsByDates);
    }
  });
  if (
    visitationsLoading ||
    historyLoading ||
    stats === null ||
    stats === undefined
  ) emitCustomEvent('loading', visitationsLoading || historyLoading);
  emitCustomEvent('loading', false);
  if (visitationsError || historyError) emitCustomEvent('error', visitationsError || historyError)
  const sessions = visitationsData?.getVisitationsForAdminPanel;

  const handleOpenClient = (index: number) => {
    setClient(sessions[index]?.client);
    setOpenClient(true);
  };
  const handleIntervalChange = (interval: Interval): void => {
    setInterval(interval)
    setStatPage(0);
  };

  const handleIntervalStyles = (type: string) => {
    return interval === type ? 'Interval-Active' : 'Interval'
  }
  const handleStats = (interval: Interval, page: number): void => {
    const intervalPerPage = (perPage: number) => {
       setDates(prevState => ({
        ...prevState,
        firstDate: dayjs().set(`${interval}`, dayjs().get(`${interval}`) - (perPage * (page + 1))).toISOString(),
        secondDate:  dayjs().set(`${interval}`, dayjs().get(`${interval}`) - (perPage * page)).toISOString(),
      }))
    }
    switch (interval) {
      case "month":
        intervalPerPage(4);
        setSeparateBy("Month");
        break;
      case  "date":
        intervalPerPage(7);
        setSeparateBy("Day");
        break;
      case "year":
        intervalPerPage(2)
        setSeparateBy("Year");
        break;
    }
  }
  if (
    stats === null ||
    stats === undefined
    ) return <></>
  const totalEarnings = Math.floor(stats?.map(stat => stat.earnings)?.reduce((acc, stat) => acc + stat, 0) || 0).toFixed(2)
  const numOfSessions = stats?.flatMap(stat => stat?.numOfSessions)?.reduce((acc, stat ) => acc + stat, 0) || 0
  return (
    <div className="History">
      {/* Header Section */}
      <div className="Header">
        <p className="Title">
          Статистика
        </p>
        <div className="Right">
        {/* Switch Dropdown */}
          <CustomSelect
            items={institutions?.map(institution => institution?.name)}
            selectItems={(item: Institution) => setSelectedInstitution(item)}
            selectedItem={selectedInstituition?.name}
          />
          {/* Date Interval Selection */}
          <div className="Intervals">
            <div
              className={handleIntervalStyles('year')}
              onClick={() => handleIntervalChange('year')}
            >
              Год
            </div>
            <div
              className={handleIntervalStyles("month")}
              onClick={()=> handleIntervalChange('month')}
            >
              Месяц
            </div>
            <div
              className={handleIntervalStyles("date")}
              onClick={()=> handleIntervalChange('date')}
            >
              День
            </div>
          </div>
        </div>
      </div>

      {/* Stats Boxes Section*/}   
      <div className="Statistics">
          {/* Stats Container */}
          <div className="Stats-Container">
            {/* <p className="Name">
              Октябрь
            </p> */}
            <button
              className='Chevron-Button'
              style={{
                justifyContent: 'flex-start',
                alignItems: 'center',
              }}
              onClick={() => setStatPage(prevState => prevState + 1)}>
              <ArrowBack/>
            </button>
              <div className="Stats">
              {stats?.slice()?.sort((aStat: Stat, bStat: Stat) => aStat?.dates[1]?.value - bStat?.dates[1]?.value
                ? aStat?.dates[1]?.value - bStat?.dates[1]?.value
                : aStat?.dates[0]?.value - bStat?.dates[0]?.value).map(
                (
                  stat: Stat,
                  index: number,
                  self: Stat[]
                ) => (
                    <div
                      className="Stat"
                      style={{ width: `${100 / self.length}%`, }}
                    >
                    <StatBox
                      earnings={stat?.earnings}
                      all={self?.length - 1}
                      people={stat?.numOfSessions}
                      last={index === self?.length - 1}
                      />
                    <p >{stat?.dates[1]?.value ? stat?.dates[1]?.value + '.' : ''}{stat?.dates[0]?.value}</p>
                  </div>
                ))}
              </div>
            <button
              className='Chevron-Button'
              style={{
                justifyContent: 'flex-end',
                alignItems: 'center',
              }}
              disabled={statPage === 0}
              onClick={() => setStatPage(prevState => prevState - 1)}>
               <ArrowForward className="Icon"/>
              </button>
          </div>
          <div className="Boxes">
            <div className="Box">
              <AvPayClient />
              <div>
                <p>Выручка</p>
                <p className="Info">
                {totalEarnings} ₸
                </p>
              </div>
            </div>
            <div className="Box">
            <AvTimeClient />
              <div>
                <p>Сессий</p>
                <p className="Info">
                  {numOfSessions}
                </p>
              </div>
            </div>
          </div>
      </div>
      
      {/* SubHeader Section */}
      <div className="Sub-Header">
        <p className="Title">
          История посещений
        </p>
        {/* Switch Dropdown */}
        <div className="Right">
          <CustomSelect
              items={institutions?.map(institution => institution?.name)}
              selectItems={(item: Institution) => setSelectedInstitution(item)}
              selectedItem={selectedInstituition?.name}
            />
        </div>
      </div>

      {/* History Cards */}
      <div className="Sessions">
        {sessions?.filter((session: Session) => (
          (selectedInstituition === undefined)
          ||
          (session?.institution?.name === selectedInstituition)
        ))
          .map((
            session: Session,
            index: number) => (
            <SessionCard
              key={index}
              session={session}
            />
          ))}
      </div>
    </div>
  )
}

export default HistoryPage

interface StatBoxProps {
  earnings: number;
  people: number;
  all: number;
  last: boolean;
}
const StatBox = ({ earnings,
  people,
  all,
  last }: StatBoxProps) => {
  const width = 100 / all;
  const scale = earnings / 10;

  return (
    <div className="Stat-Box" style={{
      width: `${width}%`,
      backgroundColor: last ? '#62B4FF' : 'rgba(204, 228, 255, 1)',
      height: `${scale * 1}px`,
    }}>
      <p style={{
        color: last ? 'rgba(255, 255, 255, 1)' : 'rgba(255, 255, 255, 1)',
          
      }}>
        {earnings} ₸
      </p>
      <p style={{
        color: last ? 'rgba(255, 255, 255, 1)' : 'rgba(255, 255, 255, 1)',
      }}>
        {people} чел.
      </p>
    </div>
  )
}



interface Props {
  session: Session;
}

const SessionCard = ({ session } : Props) => {
  const { client, institution, startTime, endTime, price, status } = session;
  return (
    <div className="Session-Card">
      <div className="Left">
        <div className="">
          <img src={avatar} alt="avatar-history" />
          <div className="Info">
            <p className="Name">
              {client?.fullName}
            </p>
            <p >
              {client?.phoneNumber}
            </p>
          </div>
        </div>
      <div className="Center">
        {/* Club Thing */}
        <p>
          {institution?.name}
        </p>
        <p>
          {institution?.address}
        </p>
      </div>
      </div>

      <div className="Right">
        <p className="Start-Time">
          {dayjs(startTime).format('HH:MM DD/MM/YYYY')}
        </p>
        <div className="">
          <p >
            {dayjs(endTime).diff(startTime, 'minutes') + ':' + dayjs(endTime).diff(startTime, 'seconds')}
          </p>
          <p >
            {price || 0} ₸ 
          </p>
        </div>
      </div>
    </div>
  )
}
