import React from 'react'
import { GET_WORKERS } from '../graphql/query/getWorkers';
import { GET_INSTITUTION_WORKER } from '../graphql/query/getInstitutionWorker';
import { useQuery } from '@apollo/client';
import { emitCustomEvent } from 'react-custom-events';

import useLocalStorage from '../hooks/useLocalStorage';
import { useInstitution } from '../utils/recoil/instituition.hook';
import { Institution } from '../interfaces/institution';
import { Worker } from '../interfaces/worker';
import {ReactComponent as ActionPlus} from '../images/Specs/ActionsPlus.svg'
import ClubCard from '../components/ClubCard';
import WorkerCard from '../components/WorkerCard';
import CreateClub from './ClubModal/CreateClub';
import CreateWorker from './ClubModal/CreateWorker';
import ViewClub from './ClubModal/ViewClub';

import '../styles/Club.scss';

const Club: React.FC = () => {
  const [store] = useLocalStorage();
  const [institutions, { setInstitution }] = useInstitution();
  const [openAddClub, setOpenAddClub] = React.useState<boolean>(false);
  const [openAddWorker, setOpenAddWorker] = React.useState<boolean>(false);
  const [openViewClub, setOpenViewClub] = React.useState<string | null>(null);

  const handleOpenCloseClub = () => {
    setOpenAddClub(!openAddClub)
  }
  const handleOpenCloseWorker = () => {
    setOpenAddWorker(!openAddWorker)
  }
  const {
    data: workerData,
    loading: workerLoading,
    error: workerError
  } = useQuery(GET_WORKERS, {
    context: {
      headers: {
        Authorization: store.getItem("JWT"),
      }
    }
  })

  // if (store.getItem("USER") === "Worker") {
  //   getInstitutionWorker()
  //   if (instWorkerLoading) emitCustomEvent('loading', workerLoading);
  //   emitCustomEvent('loading', false);
  //   if (instWorkerError) emitCustomEvent('error', workerError);
  //   const institution: Institution = instWorkerData?.getInstitutionOfWorker;
  //   return <ViewClub
  //     setOpen={() => setOpenViewClub(null)}
  //     props={institution}
  //   />
  // };
  if (workerLoading)  emitCustomEvent('loading', workerLoading);
  emitCustomEvent('loading', false);
  if (workerError) emitCustomEvent('error', workerError);

  const workers: Worker[] = workerData?.getWorkersOfAnOwner
  if (openAddClub) return (<CreateClub setOpen={handleOpenCloseClub} />)
  if (openAddWorker) return (<CreateWorker setOpen={handleOpenCloseWorker} institutions={institutions} />)
  if (openViewClub) return (
    <ViewClub
      setOpen={() => setOpenViewClub(null)}
      props={institutions?.filter((institution: Institution) => institution?._id === openViewClub)[0]} />
  )
  return (
    <div className="Club">
        {/* Clubs Header */}
      <div className="Header">
        <p>Объекты</p>
        <button className="Button" onClick={handleOpenCloseClub}>
          <ActionPlus/>
          <p>Добавить</p>
        </button>
      </div>
        {/* Clubs Container */}
      <div className="Club-Grid">
          {institutions?.map((institution: Institution, index: number) => (
              <ClubCard
                key={index}
                props={institution}
                openClub={(clubId: string) => setOpenViewClub(clubId)}
              />
          ))}
        </div>
        {/* Workers Header */}
        <div className="Header">
          <p>Сотрудники</p>
          <button className="Button" onClick={handleOpenCloseWorker} >
            <ActionPlus/>
            <p>Добавить</p>
          </button>
        </div>
        {/* Workers Container */}
        <div className="Worker-Grid">
          {workers?.map((worker: Worker, index: number) => (
            <WorkerCard props={worker} />
          ))}
        {openAddWorker && (<CreateWorker setOpen={handleOpenCloseWorker} institutions={institutions} />)}
      </div>
    </div>
  );
};

export default Club
