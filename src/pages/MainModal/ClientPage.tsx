import React from 'react'
import { emitCustomEvent } from 'react-custom-events';
import { useQuery, useMutation } from '@apollo/client';
import dayjs from 'dayjs';

import { END_SESSION } from '../../graphql/mutations/endSession';
import { GET_CLIENT_BY_ID } from '../../graphql/query/getClientByID';
import { GET_STATS_FOR_CLIENT } from '../../graphql/query/getStatsForClient';
import useLocalStorage from '../../hooks/useLocalStorage';
import { useInstitution } from '../../utils/recoil/instituition.hook';

import CustomSelect from '../../components/CustomSelect';

import { ReactComponent as AvPayClient } from '../../images/av_pay_client.svg';
import { ReactComponent as AvTimeClient } from '../../images/av_time_client.svg';
import { ReactComponent as EarningsClient } from '../../images/earning_by_client.svg';
import avatar from '../../images/emptyAvatar.png'
import { ReactComponent as ArrowBackSVG } from '../../images/backArrow.svg';
import { ReactComponent as ArrowDownSVG } from '../../images/arrowDown.svg';
import { ReactComponent as ArrowUpSVG } from '../../images/arrowUp.svg';
import { ReactComponent as CallSVG } from '../../images/Call.svg';
import { ReactComponent as MailSVG } from '../../images/Mail.svg';
import { formatDigits } from '../../helpers/getTime';
import { Client } from '../../interfaces/client';
import { Session } from '../../interfaces/session';
import { Institution } from '../../interfaces/institution';
import '../../styles/Client.scss';

interface Props {
  client: Client;
  close: () => void;
}

const ClientPage = ({ client, close }: Props) => {
  const [store] = useLocalStorage();
  const [institutions, { setInstitution }] = useInstitution();
  const [sessions, setSessions] = React.useState<Session[]>()
  const [selectedInstituition, setSelectedInstitution] = React.useState<Institution>();
  const [filter, setFilter] = React.useState<AllowedSortingValues>("Date");

  const [page, setPage] = React.useState<number>(1);

  const { loading, error } = useQuery(GET_CLIENT_BY_ID, {
    context: {
      headers: {
        'Authorization': store?.getItem('JWT'),
      }
    },
    variables: {
      "clientId": client?._id,
      "page": page,
      "sortBy": filter,
    },
    onCompleted: (data) => {
      setSessions(data?.getVisitationsOfAClientForAdminPanel);
      console.log(data)
    }
  })
  const {
    data: statsData,
    loading: statsLoading,
    error: statsError,
  } = useQuery(GET_STATS_FOR_CLIENT, {
    context: {
      headers: {
        'Authorization': store?.getItem('JWT'),
      },
    },
    variables: {
      "clientId": client?._id,
    },
  })
  const [endSessions, {
    loading: endLoading,
    error: endError
  }] = useMutation(END_SESSION, {
    context: {
      headers: {
        'Authorization': store?.getItem('JWT'),
      }
    },
    onCompleted: (data) => {
      emitCustomEvent('session-end-success', data);
    }
  });

  const handleSessionEnd = (
    clientId: string = "",
    sessionId: string = ""
  ) => {
    endSessions({
      variables: {
        "clientId": clientId,
        "sessionId": sessionId,
      }
    })
  };

  if (loading || endLoading || statsLoading) emitCustomEvent('loading', loading || endLoading || statsLoading);
  emitCustomEvent('loading', false);
  if (error || statsError) emitCustomEvent('error', error || statsError);
  if (endError) emitCustomEvent('session-end-error', endError);

  if (sessions === undefined) return (<></>)
  const activeSessions = sessions?.filter((session: Session) => session?.status === "Ongoing");
  const historySessions = sessions?.filter((session: Session) => session?.status === "Payed");
  console.log(activeSessions[0]._id)
  return (
    <div className="Client">
      <button
        className="Button-Back"
        onClick={() => close()}>
        <ArrowBackSVG />
        Назад
      </button>
      {/* Header Section -- Start*/}
      <div className="Header">
        {/* Client Info Paper -- Start */}
        <div className="Client-Info">
          <div className="Primary">
            <img
              className="Avatar"
              alt="Avatar"
              src={client?.photoURL?.XL || avatar} />
            <div className="Secondary">
              <p className="Name">{client?.fullName}</p>
              {
                client?.sex ?
                  (
                    <p className="">
                      {client?.sex === 'Male' ? 'Мужчина' : 'женщина'}
                    </p>
                  ) : (
                    <></>
                  )
                }
              {
                client?.dateOfBirth ? (
                  <p>
                    {',' + dayjs(new Date()).diff(dayjs(client?.dateOfBirth, 'year'))}
                  </p>
              ) : (
                  <></>
                )
              }
            </div>
          </div>
          <div className="General">
            <div className="Phone">
              <CallSVG/>
              <p>+{client?.phoneNumber}</p>
            </div>
            <div className="Email">
              <div className="SVG">
                <MailSVG />
              </div>
              <p>{client?.email || "Email отсутствует"}</p>
            </div>
          </div>
        </div>
        {/* Client Info Paper -- End */}

        {/* Money Info  -- Start*/}
        <div className="Money-Info">
          {/* Average Price / Duration */}
          <div className="Shorts">
            {/* Average Price per Visit */}
            <div className="Price">
              <div className="SVG">
                <AvPayClient />
              </div>
              <div>
                <p className="Title">
                  {statsData?.getPersonalStatsByClientIdForOwner?.averageMoneySpent || 0} ₸
                </p>
                <p>
                  Средняя цена за одно посещение
                </p>
              </div>
            </div>
            {/* Average Time per Visitation */}
            <div className="Time">
              <div className="SVG">
                <AvTimeClient />
              </div>
              <div>
                <p className="Title">
                  {statsData?.getPersonalStatsByClientIdForOwner?.averageTimeSpen || 0} мин
                </p>
                <p>
                  Среднее время одного посещения
                </p>
              </div>
            </div>
            {/* Overall Earnings of all Visitations */}
          </div>
          <div className="Long">
            <div className="SVG">
              <EarningsClient />
            </div>
            <div>
              <p className="Title">
                {statsData?.getPersonalStatsByClientIdForOwner?.moneySpent || 0} ₸
              </p>
              <p>
                Общая сумма всех посещений
              </p>
            </div>
          </div>
        </div>
      </div>
      {/* Header Section -- End */}
        
      {/* Active Sessions Secion -- Start*/}
      {activeSessions.length ? (
        <div className="Active">
          <p className="Title">
            Активные сессии
          </p>
          <ClientSessionCard
            session={activeSessions[0]}
            active={true}
            endSession={
              () => handleSessionEnd(
                client?._id,
                activeSessions[0]?._id)
            } />
        </div>
      ) : (
          <></>
      )}
      {/* Active Sessions Secion -- End */}

      {/* History Section -- Start */}
      <div className="History">
        {/* History Section | Header -- Start*/}
        <div className="Header">
          <p className="Title">
            История посещений
          </p>
          <div className="Right">
          <CustomSelect
                items={institutions.map(institution => institution.name)}
                selectItems={(item: Institution) => setSelectedInstitution(item)}
                selectedItem={selectedInstituition?.name}
            />
            <Filter
              filter={filter}
              setFilter={(type: AllowedSortingValues) => setFilter(type)} />
          </div>
        </div>
        {/* History Section | Header -- End */}
        {/*  History Section | Content -- Start  */}
        <div className="Content">
          {historySessions.length ? historySessions?.filter((session: Session) => (
            (selectedInstituition === undefined)
            ||
            (session?.institution?.name === selectedInstituition)
          ))
            .map((
              session: Session,
              index: number) => (
              <ClientSessionCard
                key={index}
                active={false}
                session={session}
                endSession={() => null} />
            )) : (
              <></>
            )
          }
        </div>
        {/*  History Section | Content -- End  */}
      </div>
      {/* History Section -- End */}
    </div>
  )
}
export default ClientPage;

interface ClientSessionCardProps {
  session: Session;
  active: boolean;
  endSession: () => void;
}

const ClientSessionCard = ({ session, active, endSession }: ClientSessionCardProps) => {
  const endTimeHours = active && !session?.endTime ?
    Math.abs(dayjs(session?.startTime).get("hours") - dayjs().get('hours')) :
    Math.abs(dayjs(session?.startTime).get("hours") - dayjs(session?.endTime).get("hours"));
  const endTimeMinutes = active && !session?.endTime ?
    Math.abs(dayjs(session?.startTime).get("minutes") - dayjs().get('minutes')) :
    Math.abs(dayjs(session?.startTime).get("minutes") - dayjs(session?.endTime).get("minutes"));
  const endTimeSeconds = active && !session?.endTime ?
    Math.abs(dayjs(session?.startTime).get("seconds") - dayjs().get('seconds')) :
    Math.abs(dayjs(session?.startTime).get("seconds") - dayjs(session?.endTime).get("seconds"));
  return (
    <div className={active ? 'SessionCard -active' : 'SessionCard'}>
      {/* Left Side */}
      <div className="Left">
        <div>
          {/* Left Part of Session Card */}
          <p>
            {session?.institution?.location?.address}
          </p>
          <p className="Name">
            {session?.institution?.name}
          </p>
        </div>
        <p className="id">
          {session?.institution?._id?.substring(0, 4)} - {session?.institution?._id?.substring(session?.institution?._id?.length - 4)}
        </p>
      </div>

      {/* Right Side */}
      <div className="Right">
        {/* Left Part of Session Card */}
        <div className="Times">
          <div className="Box">
            <p>Длительность</p>
            {
              `${formatDigits(endTimeHours)}:${formatDigits(endTimeMinutes)}:${formatDigits(endTimeSeconds)}` ||
              "00:00"
            }
          </div>
          <div className="Box">
            <p>Время начала</p>
            {dayjs(session?.startTime).format('HH:MM') || "00:00"}
          </div>
        </div>
        {/* Middle Left Part */}
        <div className="Box -Sum">
          <p>Сумма</p>
        {session?.price || 0} ₸
      </div>
      </div>
      {active && (
        <button onClick={() => endSession()}>
          Завершить
          </button>
        )}
    </div>
  )
}

type AllowedSortingValues = "Date" | "Duration" | "Price";
interface FilterProps {
  filter: AllowedSortingValues;
  setFilter: (filter:AllowedSortingValues) => void
}

const Filter = ({ filter, setFilter }: FilterProps) => {
  const [show, setShow] = React.useState(false);
  return (
    <div
      className="Filter"
      onClick={() => setShow(!show)}
    >
      <div className="Text">
        <p className="Selected">{filterNames(filter)}</p>
        {show ? (
              <ArrowUpSVG />
            ) : (
              <ArrowDownSVG />
            )
        }
      </div>
      {show && otherNames(filter).map((name: string, index: number) => (
        <p
          key={index}
          onClick={() => setFilter(namesToFiler(name))}>{name}</p>
        )) 
      }
    </div> 
  )
}
const filterNames = (type: AllowedSortingValues): string => {
  return type === "Date" ? "Сначала недавние" : type === "Duration" ? "По времени" : "По цене";
};
const otherNames = (type: AllowedSortingValues): string[] => {
  return type === "Date" ? ["По времени", "По цене"] : type === "Duration" ? ["Сначала недавние", "По цене"] : ["Сначала недавние", "По времени"]
};
const namesToFiler = (name: string): AllowedSortingValues => {
  console.log(name)
  return  name === "Сначала недавние" ? "Date" : name === "По времени" ? "Duration" : "Price" 
}