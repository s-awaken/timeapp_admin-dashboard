import React from 'react';
import { emitCustomEvent } from 'react-custom-events';
import { Box, Stack, Divider, Typography, CircularProgress} from '@mui/material';
import { useQuery, useSubscription, useLazyQuery } from '@apollo/client';

import useLocalStorage from '../hooks/useLocalStorage';
import { GET_PREV_MESSAGES } from '../graphql/query/getPrevMessages';
import { GET_ALL_CHATS } from '../graphql/query/getAllChats';

import ChatSearchInput from '../components/ChatSearchInput';
import ChatCard from '../components/ChatCard';
import ChatWindow from './ChatPage/ChatWindow';

import { Chat } from '../interfaces/chat';

const ChatPage = () => {
  const [chat, setChat] = React.useState<Chat>();
  const [chatType, setChatType] = React.useState<number>(0);
  const [store] = useLocalStorage();

  const handleChatTypeChange = (type: number) => {
    setChatType(type)
  }
  const { data: allChatData, loading: allChatLoading, error: allChatError } = useQuery(GET_ALL_CHATS,
    {
      context: {
        headers: {
          Authorization: store.getItem('JWT'),
        }
      },
      variables: {
        "page": 1,
      },
      onError: (error) => {
        emitCustomEvent('error', error);
      }
    }
  )
  if (allChatLoading) return (
    <CircularProgress sx={{
      alignSelf: 'center',
      justifySelf: 'center',
      my: 'auto',
    }} />
  );
  const handleCloseChat = () => {
    setChat(undefined);
  }
  return (
    <div>
      {/* Search Bar */}
      <ChatSearchInput />
      {/* Grid M-Section */}
      <Stack direction="row" sx={{ width: '100%', mt: '20px' }} spacing={3}>
        <Box sx={{ backgroundColor: '#fff', borderRadius: '12px', boxShadow: 2,minHeight: '800px', minWidth: '80%',}}>
          <Stack direction="column" spacing={2} alignItems="center" justifyContent="center">
          {/* Chats  | Grid Sub-Section */}
            {!chat ? allChatData?.getChatsOfAUser?.map((chat: Chat, index: number) => (
              <>
                <ChatCard chat={chat} key={index} setChat={() => setChat(chat)} user={chat?.user[0]}/>
                <Divider sx={{width: '80%', }}/>
              </>
            )) : (
                <ChatWindow chat={chat} close={() => handleCloseChat()} user={chat?.user[0]}/>
            )
          }
          </Stack>
        </Box>
      {/* Chat Types | Grid Sub-Section */}
      <Stack direction="column" justifyContent="center" spacing={1} sx={{
          backgroundColor: '#fff',
          borderRadius: "15px",
          height: '112px',
          width: '20%',
          p:'15px',
        }}>
          <Box
            sx={{
              p: '10px',
              height: '40px',
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: "6px",
              backgroundColor: chatType === 0 ? '#ffffff' : '',
              boxShadow: chatType === 0 ? 1 : '',
              color: chatType === 0 ? "" : "#007AFF",
            }}
            onClick={() => handleChatTypeChange(0)}
          >
            <Typography variant="h6" >Все чаты</Typography>
          </Box>
          <Box
            sx={{
              p: '10px',
              height: '40px',
              alignItems: 'center',
              justifyContent: 'center',
              borderRadius: "6px",
              backgroundColor: chatType === 1 ? '#ffffff' : '',
              boxShadow: chatType === 1 ? 1 : '',
              color: chatType === 1 ? "" : "#007AFF",
            }}
            onClick={() => handleChatTypeChange(1)}
          >
            <Typography variant="h6">Непрочитанные</Typography>
          </Box>
        </Stack>
      </Stack>
    </div>
  )
}
export default ChatPage;