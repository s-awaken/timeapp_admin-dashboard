import React from 'react';
import { useLazyQuery } from '@apollo/client';
import { emitCustomEvent } from 'react-custom-events';
import { ReactComponent as ArrowUpSVG } from '../images/arrowUp.svg'
import {ReactComponent as ArrowDownSVG} from '../images/arrowDown.svg'

import { useOwner } from '../utils/recoil/owner.hook';
import { LOGIN_OWNER } from '../graphql/query/loginOwner';
import { LOGIN_WORKER } from '../graphql/query/loginWorker';
import useLocalStorage from '../hooks/useLocalStorage';
import '../styles/Login.scss';
type LoginType = 'Администратор' | 'Сотрудник';

const Login: React.FC = () => {
  const [owner, { setOwner }] = useOwner();
  const [store] = useLocalStorage();
  const [loginType, setLoginType] = React.useState<LoginType>("Администратор")
  const [show, setShow] = React.useState<boolean>(false);
  const [loginData, setLoginData] = React.useState({
    login: '',
    password: '',
  });
  const [loginQueryOwner, { called, loading, error }] = useLazyQuery(LOGIN_OWNER, {
    variables: {
      login: loginData.login,
      password: loginData.password,
    },
    onCompleted: (data) => {
      setOwner(data.loginAsOwner)
      store.setItem("USER", "Owner")
      store.setItem("JWT", data.loginAsOwner.token);
      window.location.reload();
    },
  });
  const [loginQueryWorker, {
    called: calledWorker,
    loading: loadingWorker,
    error: errorWorker
  }] = useLazyQuery(LOGIN_WORKER, {
    variables: {
      login: loginData.login,
      password: loginData.password,
    },
    onCompleted: (data) => {
      setOwner(data.loginWorker)
      store.setItem("USER", "Worker")
      store.setItem("JWT", data.loginWorker.token);
      window.location.reload();
    },
  });
  const switchType = (clicked: boolean) => {
    console.log(clicked)
    if (!clicked) return loginType !== "Администратор" ? "Администратор" : "Сотрудник"
    const typeToSwitch = loginType !== "Администратор" ? "Администратор" : "Сотрудник"
    setLoginType(typeToSwitch)
    setShow(false)
    return;
  }
  console.log(loginType)

  if ((called && loading) || (calledWorker && loadingWorker)) emitCustomEvent('login-loading', loading);
  emitCustomEvent('login-loading', false);
  if (error || errorWorker) emitCustomEvent('login-error', error);
  
  return (
    <div className="Login-Background">
      <div className="Login-Select">
        <p onClick={() => setShow(!show)}>
          {loginType}
          {show ?
            (<ArrowUpSVG />) :
            (<ArrowDownSVG />)
          }
        </p>
        {show && (
          <p
            className="selected"
            onClick={() => switchType(true)}>
            {switchType(false)}
          </p>
        )
        }
      </div>
      <div className="Login">
        <p className='Title'>
          Вход
        </p>
        <p className='Title-2'>
          Войдите в аккаунт Time
        </p>
        <div >
          <input
            required
            id="email"
            placeholder="Адрес электронной почты"
            name="email"
            autoComplete="email"
            onChange={(event: any) => {
              setLoginData((prevState) => ({
                ...prevState,
                login: event.target.value,
              }))
            }}
            autoFocus
          />
          <input
            required
            name="password"
            placeholder="Пароль"
            type="password"
            id="password"
            autoComplete="current-password"
            onChange={(event: any) => {
              setLoginData((prevState) => ({
                ...prevState,
                password: event.target.value,
              }))
            }}
          />
          <button
            className="Login-Button"
            onClick={() => {
              loginType === "Администратор" ? loginQueryOwner() : loginQueryWorker()
            }}
          >
            Войти
          </button>
          <div className="Links">
            <a href="/" >
              Забыли пароль?
            </a>
            <a href="/">
              {"Регистрация"}
            </a>
          </div>
        </div>
      </div>
    </div>
  )
}

export default Login;
