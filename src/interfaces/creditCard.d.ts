export interface CreditCard {
  _id: string;
  cardHolderName: string;
  cardToken: string;
  cardType: string;
  last4Digits: string;
}