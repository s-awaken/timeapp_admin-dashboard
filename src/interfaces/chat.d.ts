import { Client } from './client';
import { Owner } from './owner';
import { Worker } from './worker';

export type User = Client | Owner | Worker;

export interface Chat {
  _id: string;
  dateAdded: string;
  lastMessage: Message;
  unViewedMessageNum: number;
  user: User[];
}

export interface Message {
  _id: string;
  dateSent: string;
  isViewed: boolean;
  text: string;
  user: User;
}