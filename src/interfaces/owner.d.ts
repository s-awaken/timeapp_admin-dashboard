import { PhotoURL } from "./photo";

export interface Owner {
  __typename: string;
  _id: string;
  photoURL: PhotoURL;
  email: string;
  fullName: string;
  phoneNumber: string;
  token: string;
  nameOfTheOrganzation: string;
}