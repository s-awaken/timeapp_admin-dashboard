import {PhotoURL} from './photo'
import { AllowedInstitutionTypes } from './institutionType';
import { Worker } from './worker';
import { Time } from './times';
import { Tags } from './tags';
export interface Institution {
  _id: string;
  avatarURL: PhotoURL;
  averagePrice: number;
  city: string;
  dateAdded: string;
  description: string;
  galleryURLs: PhotoURL[];
  location: {
    latitude: number;
    longitude: number;
    address: string;
  }
  contacts: {
    phoneNumber: string;
    instagram: string
    whatsApp: string;
    email: string;
  }
  name: string;
  rating: number;
  ratingCount: number;
  tags: Tags[];
  type: AllowedInstitutionTypes;
  workers: Worker
  numOfWorkers: number;
  videoURL: string;
  workTimes: Time[]
}