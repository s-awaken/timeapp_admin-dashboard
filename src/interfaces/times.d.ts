export interface Time {
  startTime: string;
  endTime: string;
}
export interface WorkTimes {
  startEndTimes: Time[];
}