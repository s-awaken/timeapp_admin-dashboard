import { PhotoURL } from "./photo";
import { Sex } from './sex'

export interface Client {
  __typename: string;
  _id: string;
  creditCards: CreditCard;
  dateOfBirth: string;
  email: string;
  fullName: string;
  phoneNumber: string;
  photoURL: PhotoURL;
  sex: Sex;
  token: string  ;
}