export type AllowedInstitutionTypes = 'ActiveLeisure' | 'Entertainment' | 'Fitness' | "BusinessCenter";
export interface InstitutionTypes {
  keyName: AllowedInstitutionTypes;
  locale: {
    ru?: string;
    en?: string;
  }
}