import { Client } from './client'
import { AllowedSessionStatus } from './sessionStatus'
import { Service } from './service'

export interface Session {
  _id: string;
  client: Client;
  clientId: string;
  endTime: string;
  institution: InstitutionMinute
  institutionId: string;
  price: number;
  service: Service;
  serviceId: string;
  startTime: string;
  status: AllowedSessionStatus;
}