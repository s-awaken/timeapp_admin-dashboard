import React from 'react'

interface Tab {
  component: any;
}

const useTabSwitch: (
  tabs: Tab[],
) => [
    tab: any,
    changeTab: (index: number) => void,
  ] = (
    tabs: Tab[],
  ) => {
    const [tab, setTab] = React.useState(tabs[0]);
    const changeTab: (index: number) => void = (index: number) => {
      setTab(tabs[index])
    };
  
  return [tab.component, changeTab]
}

export default useTabSwitch;