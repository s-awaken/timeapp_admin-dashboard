import React from 'react'

const useLocalStorage = () => {
  const store = React.useMemo(
    () => ({
      setItem: (key: string, value: string) => {
        localStorage.setItem(key, value)
      },
      getItem: (key: string) => {
        return localStorage.getItem(key)
      },
      removeItem: (key: string) => {
        localStorage.removeItem(key)
      },
    }), [])
  return [store]
}

export default useLocalStorage