import * as React from 'react';
import InputUnstyled, { InputUnstyledProps } from '@mui/core/InputUnstyled';
import { styled } from '@mui/system';

const StyledInputElement = styled('input')(`
  display: flex;
  justify-content: center;
  align-items: center;
  outline:none;
`);

const CustomTimeInput = React.forwardRef(function CustomInput(
  props: InputUnstyledProps,
  ref: React.ForwardedRef<HTMLDivElement>,
) {
  return (
    <InputUnstyled components={{ Input: StyledInputElement }} {...props} ref={ref} />
  );
});

export default function UnstyledInput(props: { time: string }) {
  return <CustomTimeInput aria-label="Time Input" defaultValue={props.time} type="time" />;
}