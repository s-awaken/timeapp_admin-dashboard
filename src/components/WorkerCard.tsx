import React from 'react'
import '../styles/components.scss';
import image from '../images/emptyAvatar.png'
import { Worker } from '../interfaces/worker';

interface WorkerProps {
  props: Worker;
};

const WorkerCard: React.FC<WorkerProps> = ({ props }: WorkerProps) => {
  const { fullName, institution } = props;

  return (
    <div className="WorkerCard">
      <img
        className='Avatar'
        alt={`${fullName}-avatar`}
        src={image}
      />
      <div>
          <p className="Name">{fullName}</p>
          <p>{institution?.name}</p>
        </div>
      </div>
  )
};

export default WorkerCard;
