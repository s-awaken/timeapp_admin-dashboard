import React from 'react'
import { Stack } from '@mui/material';
import { Time } from '../interfaces/times';
import dayjs from 'dayjs'
import utc from 'dayjs/plugin/utc';
dayjs.extend(utc)

// const WorkTimesContainer = (props: Props) => {
  
//   return (
//     <Stack
//       direction="row"
//       alignItems="center"
//       justifyContent="center">
//       <TimeContainer
//         disabled={props?.disabled}
//         time={props.time}
//         selectTime={(time: string) => props.select()}
//         num={props.num}
//         type="startTime"
//       />
      // {'\u2014'}
      // <TimeContainer
      //   disabled={props?.disabled}
      //   time={props.time}
      //   selectTime={props.select}
      //   num={props.num}
      //   type="endTime"
      // />
//     </Stack>
//   )
// }


interface TimeContainerProps {
  time: string
  selectTime: (time: string) => void;
  disabled?: boolean;
}

const WorkTimesContainer = ({ time, selectTime, disabled }: TimeContainerProps) => {
  return (
    <input
      disabled={disabled}
      className='WorkTime'
      type="time"
      placeholder={
        `${dayjs(time)
          .utc(true).get('hour')}
                :${dayjs(time)
          .utc(true).get('minute')}`}
      defaultValue={time}
      onChange={(event) => {
        console.log(new Date(new Date().toDateString() + " " + event.target.value).toISOString())
        selectTime(new Date(new Date().toDateString() + " " + event.target.value).toISOString())
      }}
    />
  )
}
export default WorkTimesContainer