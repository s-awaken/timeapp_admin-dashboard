import React from 'react';
import { Button } from '@mui/material';
import {ReactComponent as ActionPlus} from '../images/Specs/ActionsPlus.svg'
interface Props {
  text: string;
  onClick: () => void;
}


const CustomButton = ({ text, onClick }: Props) => {
  return (
    <Button
      startIcon={<ActionPlus />}
      variant="contained"
      sx={{
        mt: '8px',
        height: "60px",
        width: '170px',
        backgroundColor: '#007AFF',
        fontWeight: '400',
        fontSize: "18px",
        lineHeight: '22px',
        borderRadius: "15px",
        boxShadow: 0,
        textTransform: 'lowercase',
      }}
      onClick={() => onClick()}
    >
      <p style={{ textTransform: 'capitalize', }}>
        {text}
      </p>
      
    </Button>
  )
};

export default CustomButton;