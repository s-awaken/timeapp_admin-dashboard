import * as React from 'react';
import InputUnstyled, { InputUnstyledProps } from '@mui/core/InputUnstyled';
import { styled } from '@mui/system';

const StyledInputElement = styled('input')(`
  width: 640px;
  height: 56px;
  font-size: 1.75rem;
  backgroundColor: transparent;
  color:#979797;
  margin: 24px 0px;
  border: none;
  &:hover {
    outline:none;
  }
  &:focus {
    outline:none;
  }
`);

const CustomInput = React.forwardRef(function CustomInput(
  props: InputUnstyledProps,
  ref: React.ForwardedRef<HTMLDivElement>,
) {
  return (
    <InputUnstyled components={{ Input: StyledInputElement }} {...props} ref={ref} />
  );
});

export default function UnstyledInput() {
  return <CustomInput autoFocus aria-label="Search Field" placeholder="Введите название объекта" />;
}