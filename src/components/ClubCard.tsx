import React from 'react'
import dayjs from 'dayjs';
import { Institution } from '../interfaces/institution';
import { ReactComponent as StarSVG } from '../images/star.svg';
import { ReactComponent as ReviewSVG } from '../images/reviews.svg';
import utc from 'dayjs/plugin/utc'
import '../styles/components.scss';
dayjs.extend(utc)
interface ClubProps {
  props: Institution;
  openClub: (clubId: string) => void;
}

const handleTime = (hour: number, minute: number) => {
  return hour < 10 ?
    minute < 10 ?
      `0${hour}:0${minute}`
      : `0${hour}:${minute}`
    : `${hour}:0${minute}`
}

const ClubCard: React.FC<ClubProps> = ({ props, openClub }: ClubProps) => {
  const { _id, location, avatarURL, averagePrice, name, rating, numOfWorkers, ratingCount, workTimes } = props;
  return (
    <div className="ClubCard" onClick={() => openClub(_id)}>
      <img
        className="Avatar"
        src={avatarURL.XL}
        alt={name + '-image'}
      />
      <div className="Content">
        <p className="Name">
          {name}
        </p>
        {/* Info */}
        <div className="Info">
          <div className='Left'>
            <p>{location?.address}</p>
            <p>{`Команда: ${numOfWorkers || 0} человек`}</p>
          </div>
          <div className="Right">
            <p>
              Пн-Пт: {
                workTimes !== null ? handleTime(
                  dayjs(workTimes[0]?.startTime)?.utc()?.get('hours'),
                  dayjs(workTimes[0]?.startTime)?.utc()?.get('minutes')
                ) : handleTime(10, 0)
                || '9:00'} — {
                workTimes !== null ? handleTime(
                  dayjs(workTimes[0]?.endTime)?.utc()?.get('hours'),
                  dayjs(workTimes[0]?.endTime)?.utc()?.get('minutes')
                ) : handleTime(22, 0)
                || '20:00'}
            </p>
            <p>
              Сб-Вс: {
                workTimes !== null ? handleTime(
                  dayjs(workTimes[0].startTime).utc().get('hours'),
                  dayjs(workTimes[0].startTime).utc().get('minutes')
                ) : handleTime(10, 0)
                || '10:00'} — {
                workTimes !== null ? handleTime(
                  dayjs(workTimes[0].endTime).utc().get('hours'),
                  dayjs(workTimes[0].endTime).utc().get('minutes')
                ) : handleTime(22, 0)
                || '21:00'}
            </p>
          </div>
        </div>
        {/* Info Cards  */}
        <div className="Info-Cards">
          <div className="Paper -Black">
            <p className="White">
              {averagePrice} ₸/мин
            </p>
          </div >
          <div className="Paper">
            <StarSVG />
            <p className="Green">
              {Math.round(rating * 100) / 100 || '0'}
            </p>
          </div>
          <div className="Paper">
            <ReviewSVG />
            <p className="Black">
              {ratingCount || '0'}
            </p>
          </div>
        </div>

      </div>
    </div>
  )
}

export default ClubCard;