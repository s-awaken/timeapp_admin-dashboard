import * as React from 'react';
import { ReactComponent as ArrowDownSVG } from '../images/arrowDown.svg';
import { ReactComponent as ArrowUpSVG } from '../images/arrowUp.svg';
import '../styles/components.scss';

interface SelectProps {
  items: any[];
  selectedItem: any;
  selectItems: (item: any) => void;
}

const CustomSelect = ({ items, selectedItem, selectItems }: SelectProps) => {
  const [show, setShow] = React.useState<boolean>(false)
  return (
    <div
      className="CustomSelect"
      onClick={() => setShow(!show)}
    >
      <div className="Text">
        <p>Везде</p>
          {show ? (
            <ArrowUpSVG />
          ) : (
            <ArrowDownSVG />
          )
        }
      </div>
      {show &&
        items.map((item: string, index: number) => (
          <div
            key={index}
            onClick={() => {
              selectItems(item)
              setShow(false)
            }}
            className={`Item-show-${show}`}>
            {item}
          </div>
        ))
      }
    </div>
  )
}
export default CustomSelect;