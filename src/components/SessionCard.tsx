import React from 'react'
import dayjs from 'dayjs';
import '../styles/components.scss';
import avatar from '../images/emptyAvatar.png'
import { Session } from '../interfaces/session'


interface Props {
  session: Session;
  setOpen: () => void;
}

const SessionCard = ({ session, setOpen } : Props) => {
  const { client, institution, startTime, endTime, price, status } = session;
  return (
    <div
      className="Session-Card"
      onClick = {() =>  setOpen()}
    >
      {/* Right Side -- Start */}
      <div className="Right">
      {/* Client Info -- Start */}
        <div className="Client">
          <img
            className={status === "Ongoing" ? "Avatar -active" : "Avatar"}
            src={avatar}
            alt="session-avatar"
          />
          <div className="Info">
            <p className="Name">
              {client?.fullName}
            </p>
            <p className="Phone">
              {client?.phoneNumber}
            </p>
          </div>
        </div>
        {/* Client Info -- End */}

        {/* Club Info -- Start */}
        <div className="Club">
        <p>
          {institution?.name}
        </p>
        <p>
          {institution?.address}
        </p>
        </div>
      {/* Club Info -- End */}
      </div>
      {/* Right Side -- End */}

      {/* Session Info -- Start */}
      <div className="Session">
        <p className="Start-Time">
          {dayjs(startTime).format('HH:MM DD/MM/YYYY') || 0}
        </p>
        <div className="Times">
          <p >
            {(dayjs(endTime).diff(startTime, 'minutes') || 0) + ':' + (dayjs(endTime).diff(startTime, 'seconds') || 0)}
          </p>
          <p >
            {price || 0} ₸ 
          </p>
        </div>
      </div>
      {/* Session Info -- End */}
    </div>
  )
}

export default SessionCard
