import * as React from 'react';
import InputUnstyled, { InputUnstyledProps } from '@mui/core/InputUnstyled';
import { styled } from '@mui/system';

const StyledInputElement = styled('input')(`
  width: 100%;
  height: 66px;
  font-size: 1.25rem;
  font-weight: 400;
  line-height: 1.4375em;
  background: #fff;
  border: 1px solid #E5E8EC;
  border-radius: 14px;
  padding: 6px 10px;
  color: #20262D;
`);

const CustomInput = React.forwardRef(function CustomInput(
  props: InputUnstyledProps,
  ref: React.ForwardedRef<HTMLDivElement>,
) {
  return (
    <InputUnstyled components={{ Input: StyledInputElement }} {...props} ref={ref} />
  );
});

export default function UnstyledInput() {
  return <CustomInput aria-label="Search Field" placeholder="Поиск ..." />;
}