import { gql } from '@apollo/client';

export const GET_INSTITUTION_TYPES = gql`
query {
  getInstitutionTypes {
    keyName,
    locale {
      ru
    }
  }
}
`