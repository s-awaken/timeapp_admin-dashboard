import { gql } from '@apollo/client';

export const GET_MAX_PAGES = gql`
  query(
    $institutionIds: [String!]
    $currentPage: Int!
    ) {
  getMaximumPageOfVisitationsForOwner(
    institutionIds: $institutionIds
    currentPage: $currentPage
    )
}
`;