import { gql } from '@apollo/client';

export const GET_PREV_MESSAGES = gql`
  query(
    $chatId: String!
    $lastDate: DateTime!
  ) {
    getPreviousMessages(
      chatId: $chatId
      lastDate: $lastDate
    ) {
      _id
      text
      dateSent
      user {
        __typename
          ...on Client {
            _id
            fullName
          }
        __typename 
          ...on Worker {
            _id
            fullName
          }
        __typename
          ...on Owner {
            _id
            fullName
          }
      },
    }
  }
`;