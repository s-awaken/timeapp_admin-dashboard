import { gql } from '@apollo/client';

export const GET_TAGS = gql`
query(
	$institutionType: AllowedInstitutionTypes!
) {
	getTagsByInstitutionTypes (institutionType: $institutionType) {
		_id
		iconSVGPath
		name
		type
	}
}
`