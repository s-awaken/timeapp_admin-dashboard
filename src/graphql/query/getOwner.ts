import { gql } from '@apollo/client';

export const GET_OWNER = gql`
query{
  getOwner {
    email
    fullName
    nameOfTheOrganization
    phoneNumber
  }
}
`

