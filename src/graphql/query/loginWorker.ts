import { gql } from '@apollo/client';

export const LOGIN_WORKER = gql`
  query(
    $login: String!
    $password: String!
  ){
    loginWorker(
      login: $login
      password: $password
      ){
        _id
        token
        fullName
      }
  }
`;