import { gql } from '@apollo/client';

export const GET_STATS = gql`
  query(
    $firstDate: DateTime!
    $institutionIds: [String!]
    $secondDate: DateTime!
    $separateBy: AllowedStatsDateTypes!
  ) {
    getStatsByDates(
      firstDate: $firstDate
      institutionIds: $institutionIds
      secondDate: $secondDate
      separateBy: $separateBy
    ) {
      earnings
      dates {
        value
        dateType
      }
      numOfSessions
    }
  }
`;