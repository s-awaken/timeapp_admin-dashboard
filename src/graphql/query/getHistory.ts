import { gql } from '@apollo/client';

export const GET_VISITATIONS_BY_PERIODS_OF_TIME = gql`
  query(
    $firstDate: DateTime!
    $secondDate: DateTime!
    $institutionIds: [String!]
    $page: Int!
  ) {
    getVisitationsByPeriodsOfTime(
      firstDate: $firstDate
      secondDate: $secondDate
      institutionIds: $institutionIds
      page: $page
    ) {
      _id
      endTime
      startTime
      client {
        _id
        email
        fullName
      }
      price
      
    }
  }
`;
