import { gql } from '@apollo/client';

export const GET_WORKERS = gql`
  query {
    getWorkersOfAnOwner {
      fullName
      institution {
        name
      }
      login
    }
  }
`;
