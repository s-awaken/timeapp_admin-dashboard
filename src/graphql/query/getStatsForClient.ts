import { gql } from '@apollo/client';

export const GET_STATS_FOR_CLIENT = gql`
query (
  $clientId: String!
) {
  getPersonalStatsByClientIdForOwner(clientId: $clientId)  {
    averageMoneySpent,
    averageTimeSpent,
    moneySpent,
    numOfSessions
  }
}
`;