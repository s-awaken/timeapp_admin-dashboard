import { gql } from '@apollo/client';

export const GET_INSTITUTION_WORKER = gql`
query{
  getInstitutionOfWorker {
    _id
    name
    avatarURL {
      M
      XL
      thumbnail
    }
    galleryURLs{
      M
      XL
      thumbnail
    }
    city
    description
    averagePrice
    location {
      address
      latitude 
      longitude
    }
    contacts{
      email
      instagram
      whatsApp
      phoneNumber
    }
    numOfWorkers
    workers {
      fullName
    }
    videoURL
    rating
    ratingCount
  }
}`;