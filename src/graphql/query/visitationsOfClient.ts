import { gql } from '@apollo/client'

export const VISITATIONS_OF_CLIENT = gql`
  query(
    $institutionID: String
    $page: Int!
    $sortBy: AllowedSortingValues!
    $isActive: Boolean
    ) {
  getVisitationsForAdminPanel(
      institutionId: $institutionID
      page: $page
      sortBy: $sortBy
      isActive: $isActive
  ) {
    price
    clientId
    client {
      _id
      fullName
      phoneNumber
      email
      sex
      dateOfBirth
    }
    institution {
      _id
      location {
        address
      }
      name
    }
    startTime
    endTime
    status
    }
  }
`;