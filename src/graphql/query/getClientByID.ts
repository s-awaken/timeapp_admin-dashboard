import { gql } from "@apollo/client";

export const GET_CLIENT_BY_ID = gql`
  query(
  $clientId: String!
  $institutionId: String
  $isActive: Boolean
  $page: Int!
  $sortBy: AllowedSortingValues!
) {
  getVisitationsOfAClientForAdminPanel(
    clientId: $clientId
    institutionId: $institutionId
    isActive: $isActive
    page: $page
    sortBy: $sortBy
  ) {
    _id
    endTime
    startTime
    institution {
      _id
      name
      location {
        address

      }
    }
    status
    price
    service {
      _id
      name
    }
    client {
			fullName
			email
			sex
			dateOfBirth
		}
  }
}
`;
