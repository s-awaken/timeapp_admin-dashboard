import { gql } from '@apollo/client';

export const EDIT_INSTITUTION = gql`
mutation(
  $address: String
  $avatar: Upload
  $averagePrice: Int
  $city: String
  $description: String
  $gallery: [Upload!]
  $instagram: String
  $latitude: Float
  $longitude: Float
  $name: String
  $phoneNumber: String
  $tags: [String!]
  $type: AllowedInstitutionTypes
  $videoURL: String
  $whatsApp: String
  $institutionId: String!
) {
  editInstitution(
    address: $address
    avatar: $avatar
    averagePrice: $averagePrice
    city: $city
    description: $description
    gallery: $gallery
    instagram: $instagram
    latitude: $latitude
    longitude: $longitude
    name: $name
    phoneNumber: $phoneNumber
    tags: $tags
    type: $type
    videoURL: $videoURL
    whatsApp: $whatsApp
    institutionId: $institutionId
  ) {
    _id
  }
}
`;