import { gql } from '@apollo/client';

export const ADD_WORKER = gql`
  mutation(
    $fullName: String!
    $institutionId: String!
    $login: String!
    $password: String!
  ){
    addWorker(
      fullName: $fullName
      institutionId: $institutionId
      login: $login
      password: $password
    ) {
      _id
      fullName
      login
    }
  }
`;