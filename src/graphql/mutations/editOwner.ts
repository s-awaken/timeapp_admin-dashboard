import { gql } from '@apollo/client';

export const EDIT_OWNER = gql`
  mutation(
    $email: String
    $fullName: String
    $nameOfTheOrganzation: String
    $newPassword: String
    $oldPassword: String
    $typeOfBusiness: String
  ){
    editOwner(
      email: $email
      fullName: $fullName
      nameOfTheOrganization: $nameOfTheOrganzation
      newPassword: $newPassword
      oldPassword: $oldPassword
      typeOfBusiness: $typeOfBusiness
    ){ 
    email
    }
  }
`;