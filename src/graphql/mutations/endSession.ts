import { gql } from '@apollo/client';

export const END_SESSION = gql`
  mutation (
  $clientId: String!
  $sessionId: String!
) {
  endSessionForAdmin(
    clientId: $clientId
    sessionId: $sessionId
  ) {
    client {
      fullName
    }
  }
}
`;