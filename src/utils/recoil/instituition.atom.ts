import { atom } from 'recoil';
import { Institution } from '../../interfaces/institution';

export const institutionAtom = atom<Institution[]>({
  key: 'institution',
  default: [{
    _id: '',

    avatarURL: {
      M: "",
      XL: "",
      thumbnail: "",
    },
    averagePrice: 0,
    city: "",
    dateAdded: "",
    description: "",
    galleryURLs: [{
      M: "",
      XL: "",
      thumbnail: "",
    }],
    location: {
      address: "",
      latitude: 0,
      longitude: 0,
    },
    name: "",
    rating: 0,
    ratingCount: 0,
    tags: [{
      _id: '',
      name: '',
      iconSVGPath: ''
    }],
    type: "ActiveLeisure" || "Fitness" || "Entertainment",
    contacts: {
      phoneNumber: "",
      email: "",
      whatsApp: "",
      instagram: "",
    },
    workers: {
      __typename: '',
      _id: "",
      fullName: "",
      login: "",
      institution: {
        name: "",
      },
      photoURL: {
        M: '',
        XL: "",
        thumbnail: ""
      },
    },
    numOfWorkers: 0,
    videoURL: '',
    workTimes: [
      {startTime: "", endTime: ""}
    ]
  }]
})