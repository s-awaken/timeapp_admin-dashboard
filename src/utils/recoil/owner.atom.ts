import { atom } from 'recoil';
import { Owner } from '../../interfaces/owner';

export const ownerAtom = atom<Owner>({
  key: 'user',
  default: {
    __typename: '',
    _id: '',
    photoURL: {
      M: '',
      XL: '',
      thumbnail: '',
    },
    fullName: '',
    email: '',
    phoneNumber: '',
    token: '',
    nameOfTheOrganzation: "",
  },
})