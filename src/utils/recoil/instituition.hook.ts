import { useRecoilState } from 'recoil';
import { institutionAtom } from './instituition.atom';


import { Institution } from '../../interfaces/institution';

export const useInstitution: () => [
  Institution[],
  {
    setInstitution: (institution: Institution[]) => void;
  }
] = () => {
  const [institutions, setCurrentInstitution] = useRecoilState(institutionAtom);

  const setInstitution = (institutions: Institution[]) => setCurrentInstitution(institutions);

  return [
    institutions,
    {
      setInstitution,
    }
  ]
}